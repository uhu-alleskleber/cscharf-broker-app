﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.lib.DBConnector;
using Server.lib.APIConnector;

namespace Server
{
    public class BrokerService : IBrokerService
    {
        private DBConnector dbConnector;
        private APIConnector apiConnector;

        public BrokerService()
        {
            dbConnector = DBConnector.Instance;
            apiConnector = APIConnector.Instance;
        }

        //DB Connector Methoden
        public bool AddUser(string fname, string sname, string mail, string pw)
        {
            Console.WriteLine("User added");
             return dbConnector.AddUser(fname, sname, mail, pw);
        }

        public bool AddBalance(string userId, string ammount)
        {
            return dbConnector.AddBalance(userId, ammount);
        }

        public bool RemoveUser(string userId)
        {
            return dbConnector.RemoveUser(userId);
        }

        public List<List<string>> GetAllUserData()
        {
            return dbConnector.GetAllUserData();
        }

        public List<string> GetUserData(string userID)
        {
            return dbConnector.GetUserData(userID);
        }

        public int CheckCredentials(string email, string password)
        {
            return dbConnector.CheckCredentials(email, password);
        }

        public bool AddUserFavorite(string userID, string stockSymbol, string stockName, string exchange)
        {
            return dbConnector.AddUserFavorite(userID, stockSymbol, stockName, exchange);
        }

        public List<Dictionary<string, string>> GetUserFavorites(string userID)
        {
            return dbConnector.GetUserFavorites(userID);
        }

        public bool BuyStock(string userId, string symbol, string stockCourse, int quantity)
        {
            return dbConnector.BuyStock(userId, symbol, stockCourse, quantity);
        }

        public bool SellStock(string userId, string symbol, string stockCourse, int quantity)
        {
            return dbConnector.SellStock(userId, symbol, stockCourse, quantity);
        }

        public List<Dictionary<string, string>> GetUserDepot(string userId)
        {
            return dbConnector.GetUserDepot(userId);
        }


        //API Connector Methoden
        public List<Dictionary<string, string>> SearchForKeyword(string keyword)
        {
            return apiConnector.SearchForKeyword(keyword);
        }

        public List<Dictionary<string, string>> GetChartData(string symbol)
        {
            return apiConnector.GetChartData(symbol);
        }

        public Dictionary<string, string> GetLatestCourse(string symbol)
        {
            return apiConnector.GetLatestCourse(symbol);
        }

        public List<string> GetLatestNews(string symbol)
        {
            return apiConnector.GetLatestNews(symbol);            
        }

        public List<int> GetRemainingCalls()
        {
            return new List<int> { apiConnector.maxCalls, apiConnector.remainingCalls };
        }
    }

}
