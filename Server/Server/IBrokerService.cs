﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    [ServiceContract]
    public interface IBrokerService
    {
        //DB Connector Methoden
        [OperationContract]
        bool AddUser(string forename, string surname, string email, string password);

        [OperationContract]
        bool RemoveUser(string userId);

        [OperationContract]
        List<List<string>> GetAllUserData();

        [OperationContract]
        List<string> GetUserData(string userID);

        [OperationContract]
        int CheckCredentials(string email, string password);

        [OperationContract]
        bool AddUserFavorite(string userID, string stockSymbol, string stockName, string exchange);

        [OperationContract]
        bool AddBalance(string userId, string ammount);

        [OperationContract]
        List<Dictionary<string, string>> GetUserFavorites(string userId);

        [OperationContract]
        bool BuyStock(string userId, string symbol, string stockCourse, int quantity);

        [OperationContract]
        bool SellStock(string userId, string symbol, string stockCourse, int quantity);

        [OperationContract]
        List<Dictionary<string, string>> GetUserDepot(string userId);

    

        //APIConnector Methoden
        [OperationContract]
        List<Dictionary<string, string>> SearchForKeyword(string keyword);

        [OperationContract]
        List<Dictionary<string, string>> GetChartData(string symbol);

        [OperationContract]
        Dictionary<string, string> GetLatestCourse(string symbol);

        [OperationContract]
        List<string> GetLatestNews(string symbol);

        [OperationContract]
        List<int> GetRemainingCalls();

    }
}
