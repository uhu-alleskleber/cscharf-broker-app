﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server;
using Server.lib.APIConnector;
using Server.lib.DBConnector;
using Server.lib.util;
using System.ServiceModel;
using System.ServiceModel.Description;


namespace ServerGUI
{
    public class Server
    {
        public string Host;
        public int Port;
        public string ServiceName;
        public BrokerService brokerService;

        public Server(string Host, int Port, string SName)
        {
            this.Host = Host;
            this.Port = Port;
            ServiceName = SName;
            brokerService = new BrokerService();
        }
        public Server()
        {
            Host = "net.tcp://localhost";
            Port = 1337;
            ServiceName = "BrokerApplication";
            brokerService = new BrokerService();
        }
        public ServiceHost StartServer()
        {

            string svcAddress = Host + ":" + Port + "/" + ServiceName;
            Uri svcUri = new Uri(svcAddress);

            ServiceHost sh = new ServiceHost(typeof(BrokerService), svcUri);
                // Binding
                NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.Message);

                // Behavior
                ServiceMetadataBehavior behavior = new ServiceMetadataBehavior();
                sh.Description.Behaviors.Add(behavior);
                sh.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                //Endpoint
                sh.AddServiceEndpoint(typeof(IBrokerService), tcpBinding, svcAddress);

                // Open
                sh.Open();

                Console.WriteLine("Service started '" + svcAddress + "' ...Press key to quit.");
                //Console.ReadKey();

                Console.WriteLine("Quit. Press key to close.");
                //Console.ReadKey();

                // Close
                // sh.Close();  
            return sh;
        }
        public bool StoppServer(ServiceHost sh)
        {
            Console.WriteLine("Server wird gestoppt...");
            sh.Close();
            return true;
        }

    }

}
