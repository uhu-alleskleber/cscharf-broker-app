﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.lib.util
{
    class TimestampConverter
    {
        public static DateTime ToDateTime(string timestamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

            try
            {
                double unixTimeStamp = Convert.ToDouble(timestamp);                
                dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime(); 
            } catch (Exception e)
            {
                Console.WriteLine($"Fehler in TimestampConverter::ToDateTime(): {e}");
            }

            return dtDateTime;
        }

        public static string ToDateString(string timestamp)
        {
            string output = "";

            try
            {
                DateTime dt = ToDateTime(timestamp);
                output = dt.ToString("G", CultureInfo.CreateSpecificCulture("de-DE"));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fehler in TimestampConverter::ToDateString(): {e}");
            }

            return output;
        }
    }
}
