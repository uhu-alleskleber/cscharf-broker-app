﻿DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS user_favorites;
DROP TABLE IF EXISTS transaction_types;
DROP TABLE IF EXISTS transactions;

CREATE TABLE user
(
    ID       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    forename TEXT    NOT NULL,
    surname  TEXT    NOT NULL,
    email    TEXT    NOT NULL,
    password TEXT    NOT NULL, -- SHA256 Hash
	balance  DOUBLE  NOT NULL
);

CREATE TABLE user_favorites
(
    ID       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    user_id  INTEGER NOT NULL,
    stock_sc TEXT    NOT NULL,
    name	 TEXT    NOT NULL,
    exchange TEXT    NOT NULL,

    FOREIGN KEY (user_id) REFERENCES user (ID)
);

CREATE TABLE transaction_types
(
    ID        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    type_name TEXT    NOT NULL
);

CREATE TABLE transactions
(
    ID                  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    user_id             INTEGER NOT NULL,
    transaction_type_id INTEGER NOT NULL,
    stock_sc            TEXT    NOT NULL,
    stock_course        DOUBLE,
    quantity            INTEGER NOT NULL,

    FOREIGN KEY (user_id) REFERENCES user (ID),
    FOREIGN KEY (transaction_type_id) REFERENCES transaction_types (ID)
);


INSERT INTO user(forename, surname, email, password, balance)
VALUES ('Steve', 'Dietrich', 's.dietrich@noble.vip',
        '79f32b1fd9b0f5e63c46abf96a113c6c85d168fe201abb08c8c78ad57d4df074', 1000.0),
       ('Otto', 'Härtel', 'otto.haertel@tinder.com',
        'c31c49055c0e9a2fb19596ef26cad5c346c0dde750e51aced938137e4d2243ee', 1000.0),
       ('Sofya', 'Korogodskaya', 's.koro@rmail.vodka',
        '81844623fb208176bd32a2ec639b4456c5cb2e4cb40357f2cd241ca4c9b91b4e', 1000.0);

INSERT INTO user_favorites(user_id, stock_sc, name, exchange)
VALUES (1, 'TSLA', 'Tesla, Inc.', 'NMS'),
       (1, 'NVDA', 'NVIDIA Corporation', 'NMS'),
       (1, 'ABEA.F', 'ALPHABET INC.CL.A DL-,001', 'FRA'),
       (1, 'DBK.F', 'DEUTSCHE BANK AG NA O.N.', 'FRA'),
       (2, 'SIE.DE', 'SIEMENS AG  NA O.N.', 'GER'),
       (2, 'SAP.F', 'SAP SE O.N.', 'FRA'),
       (2, 'TSLA', 'Tesla, Inc.', 'NMS'),
       (2, 'CCC3.F', 'COCA-COLA CO.  DL-,25', 'FRA'),
       (3, 'SIE.DE', 'SIEMENS AG  NA O.N.', 'GER'),
       (3, 'BYW6.F', 'BAYWA AG VINK.NA. O.N.', 'FRA'),
       (3, 'SAP.F', 'SAP SE O.N.', 'FRA'),
       (3, 'TSLA', 'Tesla, Inc.', 'NMS');

INSERT INTO transaction_types(type_name)
VALUES ('Buy'),
       ('Sell');

INSERT INTO transactions(user_id, transaction_type_id, stock_sc, stock_course, quantity )
VALUES (2, 1, 'SAP.F', 100.0, 5),
	   (2, 1, 'SIE.DE', 100.0, 3);

