﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using Server.lib.util;


namespace Server.lib.DBConnector
{
    public class DBConnector
    {
        readonly string dbName = "stonks.db";
        APIConnector.APIConnector apiConnector;
        SQLiteConnection conn;

        private DBConnector()
        {
            conn = CreateConnection();
            apiConnector = APIConnector.APIConnector.Instance;
        }

        private SQLiteConnection CreateConnection()
        {
            try
            {
                if (File.Exists(dbName))
                {
                    Console.WriteLine($"Database {dbName} already exists...");
                    conn = new SQLiteConnection("Data Source=stonks.db;Version=3;New=True;Compress=True");
                    conn.Open();
                    Console.WriteLine($"Connection to {dbName} established");
                }
                else
                {
                    Console.WriteLine($"Database {dbName} do no exists, it will be initialized...");
                    conn = new SQLiteConnection("Data Source=stonks.db;Version=3;New=True;Compress=True");
                    conn.Open();
                    Console.WriteLine($"Connection to {dbName} established");
                    InitializeDB();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Databse connection failed!:{e}");
            }

            return conn;
        }

        public void InitializeDB()
        {
            string sqlPath = @"..\..\..\Server\lib\DBConnector\init.sql";
            SQLiteCommand query = conn.CreateCommand();

            Console.WriteLine($"Initializing Database from Sql-File '{sqlPath}'");

            query.CommandText = System.IO.File.ReadAllText(sqlPath);
            query.ExecuteNonQuery();
        }

        public List<List<string>> GetAllUserData()
        {
            /*
             Returns a List, comtainging Lists with all User Data 
             */
            Console.WriteLine("Run Method DBConnector::GetAllUserData()...");

            List<List<string>> output = new List<List<string>>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = "SELECT ID, forename, surname, email, balance FROM user";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                List<string> userData = new List<string>();

                string ID = datareader.GetInt64(0).ToString();
                string forename = datareader.GetString(1);
                string surname = datareader.GetString(2);
                string email = datareader.GetString(3);
                string balance = datareader.GetDouble(4).ToString();

                userData.Add(ID);
                userData.Add(forename);
                userData.Add(surname);
                userData.Add(email);
                userData.Add(balance);

                output.Add(userData);
            }

            return output;
        }

        public bool AddUser(string forename, string surname, string email, string password)
        {
            /*
             Inserts new User into Database if User doesnt exist; returns true if succeeded
             IMPORTANT: Method takes unhashed password
             */
            Console.WriteLine("Run Method DBConnector::AddUser()...");

            string pwHash;

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            // 1. Check if User exists        
            query.CommandText = $"SELECT ID FROM user WHERE email = '{email}'";

            datareader = query.ExecuteReader();
            if (datareader.Read())
                // Email existiert schon in DB
                return false;
            datareader.Close();

            // 2. Insert User
            pwHash = HashConverter.ComputeSHA256Hash(password);
            query.CommandText = $@"INSERT INTO user(forename, surname, email, password, balance)
                                  VALUES ('{forename}', '{surname}', '{email}', '{pwHash}', 1000.0)";

            query.ExecuteNonQuery();

            MailSender.MailSender.sendRegisterMail(email, surname, forename);

            return true;
        }

        public bool AddBalance(string userId, string ammount)
        {
            /*
             Updates balance column for user id in table user
             */
            Console.WriteLine("Run Method DBConnector::AddBalance()...");
            
            SQLiteCommand query = conn.CreateCommand();
     
            query.CommandText = $"UPDATE user SET balance = (SELECT balance FROM user WHERE ID = {userId}) + {ammount} WHERE ID = {userId}";

            query.ExecuteNonQuery();

            return true;
        }

        public bool RemoveUser(string userId)
        {
            /*
             Deltes User from DB
             */
            Console.WriteLine("Run Method DBConnector::RemoveUser()...");

            SQLiteCommand query = conn.CreateCommand();
    
            query.CommandText = $"DELETE FROM user WHERE ID = {userId}";
            query.ExecuteNonQuery();

            return true;
        }

        public List<string> GetUserData(string userID)
        {
            /*
             Returns a List with all user data for given userID
             */
            Console.WriteLine("Run Method DBConnector::GetUserByID()...");

            List<string> output = new List<string>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $"SELECT ID, forename, surname, email, balance FROM user WHERE ID = {userID}";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                string ID = datareader.GetInt64(0).ToString();
                string forename = datareader.GetString(1);
                string surname = datareader.GetString(2);
                string email = datareader.GetString(3);
                string balance = datareader.GetDouble(4).ToString();

                output.Add(ID);
                output.Add(forename);
                output.Add(surname);
                output.Add(email);
                output.Add(balance);
            }

            return output;
        }

        public int CheckCredentials(string email, string password)
        {
            /*
             Returns a bool value if the given login credentials are correct
             */
            Console.WriteLine("Run Method DBConnector::CheckCredentials()...");

            int output;
            string pwHash = HashConverter.ComputeSHA256Hash(password);

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $"SELECT ID FROM user WHERE email = '{email}' AND password = '{pwHash}'";

            datareader = query.ExecuteReader();

            output = datareader.Read() ? datareader.GetInt32(0) : 0;

            return output;
        }

        public bool AddUserFavorite(string userID, string stockSymbol, string stockName, string exchange)
        {
            /*
             Inserts a new User Stock Favorite 
             */
            Console.WriteLine("Run Method DBConnector::AddUserFavorite()...");

            SQLiteCommand query = conn.CreateCommand();

            try
            {
                query.CommandText = $"INSERT INTO user_favorites(user_id, stock_sc, name, exchange) VALUES ({userID}, '{stockSymbol}', '{stockName}', '{exchange}')";
                query.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public List<Dictionary<string, string>> GetUserFavorites(string userID)
        {
            /*
             Returns a List, comtaining all User Favorite stock symbols
             */
            Console.WriteLine("Run Method DBConnector::GetUserFavorites()...");

            List<Dictionary<string, string>> output = new List<Dictionary<string, string>>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $"SELECT stock_sc, name, exchange FROM user_favorites WHERE user_id = {userID}";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                Dictionary<string, string> stockFavorite = new Dictionary<string, string>();

                string symbol = datareader.GetString(0);
                string name = datareader.GetString(1);
                string exchange = datareader.GetString(2);

                stockFavorite["symbol"] = symbol;
                stockFavorite["name"] = name;
                stockFavorite["exchange"] = exchange;

                output.Add(stockFavorite);
            }

            return output;
        }

        public bool BuyStock(string userId, string symbol, string stockCourse, int quantity)
        {
            /*
             Inserts a new Transaction (Buy, 1) into Database
             */
            Console.WriteLine("Run Method DBConnector::BuyStock()...");

            SQLiteCommand query = conn.CreateCommand();

            try
            {
                query.CommandText = $@"INSERT INTO transactions(user_id, transaction_type_id, stock_sc, stock_course, quantity)
                                       VALUES ({userId}, 1, '{symbol}', {stockCourse}, {quantity})";
                query.ExecuteNonQuery();

                query.CommandText = $@"UPDATE user SET balance = (SELECT balance FROM user WHERE ID = {userId}) - ({stockCourse} * {quantity}) WHERE ID = {userId}";
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error in DBConnector::BuyStock(): {e}");
                return false;
            }

            try
            {
                List<string> userData = GetUserData(userId);
                string mail = userData[3];
                string forename = userData[1];
                string surname = userData[2];
                double course = Convert.ToDouble(stockCourse);
                MailSender.MailSender.sendBuySellMail(mail, surname, forename, 1, symbol, course, quantity);
            }
            catch
            {
                Console.WriteLine("Mail konnte nicht gesendet werden.");
            }

            return true;
        }

        public bool SellStock(string userId, string symbol, string stockCourse, int quantity)
        {
            /*
             Inserts a new Transaction (Sell, 2) into Database
             */
            Console.WriteLine("Run Method DBConnector::SellStock()...");

            SQLiteCommand query = conn.CreateCommand();

            try
            {
                query.CommandText = $@"INSERT INTO transactions(user_id, transaction_type_id, stock_sc, stock_course, quantity)
                                       VALUES ({userId}, 2, '{symbol}', {stockCourse}, {quantity})";
                query.ExecuteNonQuery();

                query.CommandText = $@"UPDATE user SET balance = (SELECT balance FROM user WHERE ID = {userId}) + ({stockCourse} * {quantity}) WHERE ID = {userId}";
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fehler in DBConnector::SellStock(): {e}");
                Console.WriteLine($"Fehler in DBConnector::SellStock(): {e}");
                return false;
            }

            try
            {
                List<string> userData = GetUserData(userId);
                string mail = userData[3];
                string forename = userData[1];
                string surname = userData[2];
                double course = Convert.ToDouble(stockCourse);
                MailSender.MailSender.sendBuySellMail(mail, surname, forename, 2, symbol, course, quantity);
            }
            catch
            {
                Console.WriteLine("Mail konnte nicht gesendet werden.");
            }

            return true;
        }

        public List<Dictionary<string, string>> GetUserDepot(string userId)
        {
            /*
             Returns a List, comtaining the UserDepot
             */
            Console.WriteLine("Run Method DBConnector::GetUserDepot()...");

            List<Dictionary<string, string>> output = new List<Dictionary<string, string>>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $@"SELECT  
		                            stock_sc,
		                            SUM(CASE WHEN transaction_type_id = 1 THEN quantity ELSE 0 END) -
                                    SUM(CASE WHEN transaction_type_id = 2 THEN quantity ELSE 0 END)
                                        AS sum
                                    FROM    transactions
                                    WHERE   user_id = {userId}
                                    GROUP BY stock_sc";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                Dictionary<string, string> stock = new Dictionary<string, string>();

                string symbol = datareader.GetString(0);
                string quantity = datareader.GetInt32(1).ToString();

                stock["symbol"] = symbol;
                stock["quantity"] = quantity;

                if (Convert.ToInt32(quantity) > 0)
                    output.Add(stock);
            }

            return output;
        }


        // Implementing Singleton pattern
        public static DBConnector Instance
        {
            get { return lazy.Value; }
        }

        private static readonly Lazy<DBConnector> lazy = new Lazy<DBConnector>(() => new DBConnector());

    }
}
