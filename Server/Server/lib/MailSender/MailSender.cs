﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Server.lib.MailSender
{
    class MailSender
    {
        static string smtpServer = "mail.gmx.net";
        static string sender = "stonks-broker@gmx.de";
        static string password = "stonksBrokerApp2020";

        public static void sendBuySellMail(string to, string surname, string forename, int act, string symbol, double course, int quantity)
        {
            string action;

            if (act == 1)
                action = "Kauf";
            else
                action = "Verkauf";

            MailMessage message = new MailMessage(sender, to);
            message.Subject = $"{action} von {symbol}";
            message.Body = System.IO.File.ReadAllText(@"..\..\..\Server\lib\MailSender\templates\sell-buy.html");
            message.Body = message.Body.Replace("%SURNAME%", surname);
            message.Body = message.Body.Replace("%FORENAME%", forename);
            message.Body = message.Body.Replace("%QUANTITY%", quantity.ToString());
            message.Body = message.Body.Replace("%SYMBOL%", symbol);
            message.Body = message.Body.Replace("%COURSE%", course.ToString());
            message.Body = message.Body.Replace("%TOTAL%", (course*quantity).ToString());

            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient(smtpServer);
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential(sender, password);
            client.EnableSsl = true;
            // client.UseDefaultCredentials = true;

            client.Send(message);

        }

        public static void sendRegisterMail(string to, string surname, string forename)
        {

            MailMessage message = new MailMessage(sender, to);
            message.Subject = $"Registrierung";
            message.Body = System.IO.File.ReadAllText(@"..\..\..\Server\lib\MailSender\templates\register.html");
            message.Body = message.Body.Replace("%SURNAME%", surname);
            message.Body = message.Body.Replace("%FORENAME%", forename);

            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient(smtpServer);
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential(sender, password);
            client.EnableSsl = true;
            // client.UseDefaultCredentials = true;

            client.Send(message);

        }
    }
}
