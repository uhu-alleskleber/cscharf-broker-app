﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.lib.APIConnector
{
    public class APIConnector
    {
        public int maxCalls;
        public int remainingCalls;
        readonly string rapidApiHost = "apidojo-yahoo-finance-v1.p.rapidapi.com";
        private string apiKeyPath = @"..\..\..\Server\lib\APIConnector\APIKey.txt";
        public string rapidApiKey;
        RestRequest request;

        private APIConnector()
        {
            rapidApiKey = System.IO.File.ReadAllText(apiKeyPath);

            request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", rapidApiHost);
            request.AddHeader("x-rapidapi-key", rapidApiKey);

            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            // Test Call, um maxCalls und remainingCalls zu füllen
            SearchForKeyword("Siemens");
        }

        public List<Dictionary<string, string>> SearchForKeyword(string keyword)
        {
            /*
             Returns a List of Dictionarys in form (json) (example COCA-Cola (CCC3)): 
             [
                {
                    'symbol': 'CCC3.F',
                    'name': 'COCA-COLA CO.  DL-,25',
                    'excange': 'FRA',
                    'score': '20048.0'
                },
                {
                    'symbol': 'CCC3.BE',
                    'name': 'COCA-COLA CO.  DL-,25',
                    'excange': 'BER',
                    'score': '20004.0'
                },
                ...
             ] 
             */
            Console.WriteLine("Run Method APIConnector::SearchForKeyword()...");

            List<Dictionary<string, string>> output = new List<Dictionary<string, string>>();

            string url = $"https://rapidapi.p.rapidapi.com/auto-complete?q={keyword}&region=DE";

            RestClient client = new RestClient(url);
            IRestResponse response = client.Execute(request);
            SetRemainingCalls(response);

            try
            {
                dynamic objects = JsonConvert.DeserializeObject(response.Content);
                foreach (var q in objects.quotes)
                {
                    Dictionary<string, string> quote = new Dictionary<string, string>();

                    string symbol = q.symbol;
                    string name = q.shortname;
                    string exchange = q.exchange;
                    string score = q.score.ToString();

                    quote["symbol"] = symbol;
                    quote["name"] = name;
                    quote["exchange"] = exchange;
                    quote["score"] = score;

                    output.Add(quote);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception in APIConnector::SearchForKeyword(): {e}");
            }

            return output;
        }

        public List<Dictionary<string, string>> GetChartData(string symbol)
        {
            /*
             Returns a List of Dictionaries fot plotting a stock chart in 5 min interval for the last 5 days. 
             Timestamp is in Unix format and course must be casted to double
             Example:
             [
                {
                    'currency': 'EUR',
                    'timestamp': '1602658800',
                    'course': '111.41999816894531'
                },
                {
                    'currency': 'EUR',
                    'timestamp': '1602659100',
                    'course': '111.5'
                },
                {
                    'currency': 'EUR',
                    'timestamp': '1602659400',
                    'course': '111.4800033569336'
                },
                ...
             ] 
             */
            Console.WriteLine("Run Method APIConnector::GetChartData()...");

            List<Dictionary<string, string>> output = new List<Dictionary<string, string>>();

            string url = $"https://rapidapi.p.rapidapi.com/market/get-charts?symbol={symbol}&interval=1d&range=1mo&region=DE";

            RestClient client = new RestClient(url);
            IRestResponse response = client.Execute(request);

            try
            {
                dynamic objects = JsonConvert.DeserializeObject(response.Content);
                int len = objects.chart.result[0].timestamp.Count;
                Console.WriteLine("HIER LAENGE" + len);
                for (int i = 0; i < len; i++)
                {
                    Dictionary<string, string> value = new Dictionary<string, string>();

                    string currency = objects.chart.result[0].meta.currency.ToString();
                    string timestamp = objects.chart.result[0].timestamp[i].ToString();
                    string course = objects.chart.result[0].indicators.quote[0].close[i].ToString();

                    //if (course.Length == 0)
                    //    course = "0.0";
                    
                    value["currency"] = currency;
                    value["timestamp"] = timestamp;
                    value["course"] = course;

                    if (course.Length != 0)  // Nur hinzufügen, wenn API nicht NULL zurückgibt
                        output.Add(value);
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception in APIConnector::GetChartData(): {e}");
            }
            int maxSize = 50;
            if (output.Count > maxSize)
                return output.GetRange(output.Count - maxSize, maxSize - 1);
            else
                return output;


        }

        public Dictionary<string, string> GetLatestCourse(string symbol)
        {
            /*
             Returns Dictionary of latest course
             Example:
             {
                'currency': 'EUR',
                'course': '111.41999816894531',
                'change': '0.64'
             }
             */          //  regularMarketPrice
            Console.WriteLine("Run Method APIConnector::GetLatestCourse()...");

            Dictionary<string, string> output = new Dictionary<string, string>();

            string url = $"https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes?symbols={symbol}&region=DE";

            RestClient client = new RestClient(url);
            IRestResponse response = client.Execute(request);

            try
            {
                dynamic objects = JsonConvert.DeserializeObject(response.Content);

                string currency = objects.quoteResponse.result[0].currency;
                string course = objects.quoteResponse.result[0].regularMarketPrice.ToString();
                string change = objects.quoteResponse.result[0].regularMarketChange.ToString();

                output["currency"] = currency;
                output["course"] = course;
                output["change"] = change;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception in APIConnector::GetLatestCourse(): {e}");
            }

            return output;
        }

        public List<string> GetLatestNews(string symbol)
        {
            /*
             Returns a List of contents of 1 recent news article for a given stock symbol 
            */
            List<string> output = new List<string>();

            string url = $"https://rapidapi.p.rapidapi.com/stock/get-news?category={symbol}&region=DE"; // Eigentlich Depricated

            RestClient client = new RestClient(url);
            IRestResponse response = client.Execute(request);

            try
            {
                dynamic objects = JsonConvert.DeserializeObject(response.Content);
                if (objects.items.result.Count != 0)
                {

                    string title = objects.items.result[0].title;
                    string link = objects.items.result[0].link;
                    string content = "Hier die News Bla Bla"; //objects.items.result[0].content;
                    string timestamp = objects.items.result[0].published_at.ToString();

                    output.Add(title);
                    output.Add(link);
                    output.Add(content);
                    output.Add(timestamp);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception in APIConnector::GetLatestNews(): {e}");
            }

            return output;
        }

        private void SetRemainingCalls(IRestResponse response)
        {
            int remainingCalls = Convert.ToInt16(response.Headers.ToList()
                                                .Find(x => x.Name == "X-RateLimit-requests-Remaining")
                                                .Value);

            int maxCalls = Convert.ToInt16(response.Headers.ToList()
                                                .Find(x => x.Name == "X-RateLimit-requests-Limit")
                                                .Value);

            this.maxCalls = maxCalls;
            this.remainingCalls = remainingCalls;
        }

        // Implementing Singleton pattern
        public static APIConnector Instance
        {
            get { return lazy.Value; }
        }

        private static readonly Lazy<APIConnector> lazy = new Lazy<APIConnector>(() => new APIConnector());
    }
}
