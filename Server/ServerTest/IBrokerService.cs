﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ServerTest
{
    [ServiceContract]
    public interface IBrokerService
    {

        //DBConnector dbConnector { get; set; }
        //APIConnector apiConnector { get; set; }

        //DB Connector Methoden
        [OperationContract]
        bool AddUser(string forename, string surname, string email, string password);

        [OperationContract]
        List<List<string>> GetAllUserData();

        [OperationContract]
        List<string> GetUserData(string userID);

        [OperationContract]
        bool CheckCredentials(string email, string password);

        [OperationContract]
        bool AddUserFavorite(string userId, string stockSymbol);

        [OperationContract]
        List<string> GetUserFavorites(string userId);

        bool BuyStock(string userId, string symbol, double stockCourse, int quantity);

        bool SellStock(string userId, string symbol, double stockCourse, int quantity);


        //APIConnector Methoden
        [OperationContract]
        List<Dictionary<string, string>> SearchForKeyword(string keyword);

        [OperationContract]
        List<Dictionary<string, string>> GetChartData(string symbol);

        [OperationContract]
        Dictionary<string, string> GetLatestCourse(string symbol);

        [OperationContract]
        List<string> GetLatestNews(string symbol);

    }
}
