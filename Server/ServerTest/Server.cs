﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerTest;
using ServerTest.lib.APIConnector;
using ServerTest.lib.DBConnector;
using ServerTest.lib.util;
using System.ServiceModel;
using System.ServiceModel.Description;


namespace ServerTest
{
    public class Server
    {
        public String Host;
        public Int32 Port;
        public String ServiceName;
        public BrokerService brokerService;

        public Server(string Host, int Port, string SName)
        {
            this.Host = Host;
            this.Port = Port;
            ServiceName = SName;
            brokerService = new BrokerService();
        }
        public Server()
        {
            Host = "net.tcp://localhost";
            Port = 31227;
            ServiceName = "Broker-APP";
            brokerService = new BrokerService();
        }
        public bool StartServer()
        {

            string svcAddress = Host + ":" + Port + "/" + ServiceName;
            Uri svcUri = new Uri(svcAddress);
            
            using (ServiceHost sh = new ServiceHost(typeof(BrokerService), svcUri))
            {
                // Binding
                NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.Message);

                // Behavior
                ServiceMetadataBehavior behavior = new ServiceMetadataBehavior();
                sh.Description.Behaviors.Add(behavior);
                sh.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                //Endpoint
                sh.AddServiceEndpoint(typeof(IBrokerService), tcpBinding, svcAddress);

                // Open
                sh.Open();

                Console.WriteLine("Service started '" + svcAddress + "' ...Press key to quit.");
                //Console.ReadKey();

                Console.WriteLine("Quit. Press key to close.");
                //Console.ReadKey();

                // Close
                // sh.Close();  
            }
            return true;
        }
        public bool StoppServer()
        {
            return true;
        }

    }

}
