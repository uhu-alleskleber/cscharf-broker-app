﻿DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS user_favorites;
DROP TABLE IF EXISTS transaction_types;
DROP TABLE IF EXISTS transactions;

CREATE TABLE user
(
    ID       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    forename TEXT    NOT NULL,
    surname  TEXT    NOT NULL,
    email    TEXT    NOT NULL,
    password TEXT    NOT NULL, -- SHA256 Hash
	balance  DOUBLE  
);

CREATE TABLE user_favorites
(
    ID       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    user_id  INTEGER NOT NULL,
    stock_sc TEXT    NOT NULL,

    FOREIGN KEY (user_id) REFERENCES user (ID)
);

CREATE TABLE transaction_types
(
    ID        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    type_name TEXT    NOT NULL
);

CREATE TABLE transactions
(
    ID                  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    user_id             INTEGER NOT NULL,
    transaction_type_id INTEGER NOT NULL,
    stock_sc            TEXT    NOT NULL,
    stock_course        DOUBLE  NOT NULL,
    quantity            INTEGER NOT NULL,

    FOREIGN KEY (user_id) REFERENCES user (ID),
    FOREIGN KEY (transaction_type_id) REFERENCES transaction_types (ID)
);


INSERT INTO user(forename, surname, email, password)
VALUES ('Steve', 'Dietrich', 's.dietrich@noble.vip',
        '79f32b1fd9b0f5e63c46abf96a113c6c85d168fe201abb08c8c78ad57d4df074'),
       ('Pascal', 'Fabiano', 'pascal.fabiano@oython.fangirl',
        '1fddae0494c779d0868a2a2a40ad778f71aa9206ea807a4d7a3fd7af05120d42'),
       ('Otto', 'Härtel', 'otto.haertel@tinder.com',
        'c31c49055c0e9a2fb19596ef26cad5c346c0dde750e51aced938137e4d2243ee'),
       ('Sofya', 'Korogodskaya', 's.koro@rmail.vodka',
        '81844623fb208176bd32a2ec639b4456c5cb2e4cb40357f2cd241ca4c9b91b4e');

INSERT INTO user_favorites(user_id, stock_sc)
VALUES (1, 'TSLA'),
       (1, 'NVDA'),
       (1, 'ABEA.F'),
       (1, 'DBK.F'),
       (2, 'SIE.DE'),
       (2, 'SAP.F'),
       (2, 'TSLA'),
       (2, 'CCC3.F'),
       (3, 'SIE.DE'),
       (3, 'BYW6.F'),
       (3, 'SAP.F'),
       (3, 'TSLA'),
       (4, 'MTT.DE'),
       (4, 'KMY.F'),
       (4, 'SIE.DE'),
       (4, 'NESM.F');

INSERT INTO transaction_types(type_name)
VALUES ('Buy'),
       ('Sell')
