﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using ServerTest.lib.util;


namespace ServerTest.lib.DBConnector
{
    public class DBConnector
    {
        readonly string dbName = "stonks.db";
        APIConnector.APIConnector apiConnector;
        SQLiteConnection conn;

        private DBConnector()
        {
            conn = CreateConnection();
            apiConnector = APIConnector.APIConnector.Instance;
        }

        private SQLiteConnection CreateConnection()
        {
            try
            {
                if (File.Exists(dbName))
                {
                    Console.WriteLine($"Database {dbName} already exists...");
                    conn = new SQLiteConnection("Data Source=stonks.db;Version=3;New=True;Compress=True");
                    conn.Open();
                    Console.WriteLine($"Connection to {dbName} established");
                }
                else
                {
                    Console.WriteLine($"Database {dbName} do no exists, it will be initialized...");
                    conn = new SQLiteConnection("Data Source=stonks.db;Version=3;New=True;Compress=True");
                    conn.Open();
                    Console.WriteLine($"Connection to {dbName} established");
                    InitializeDB();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Databse connection failed!:{e}");
            }



            return conn;
        }

        private void InitializeDB()
        {
            string sqlPath = @"..\..\..\Server\lib\DBConnector\init.sql";
            SQLiteCommand query = conn.CreateCommand();

            Console.WriteLine($"Initializing Database from Sql-File '{sqlPath}'");

            query.CommandText = System.IO.File.ReadAllText(sqlPath);
            query.ExecuteNonQuery();
        }

        public List<List<string>> GetAllUserData()
        {
            /*
             Returns a List, comtainging Lists with all User Data 
             */
            Console.WriteLine("Run Method DBConnector::GetAllUserData()...");

            List<List<string>> output = new List<List<string>>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = "SELECT ID, forename, surname, email FROM user";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                List<string> userData = new List<string>();

                string ID = datareader.GetInt64(0).ToString();
                string forename = datareader.GetString(1);
                string surname = datareader.GetString(2);
                string email = datareader.GetString(3);

                userData.Add(ID);
                userData.Add(forename);
                userData.Add(surname);
                userData.Add(email);

                output.Add(userData);
            }

            return output;
        }

        public bool AddUser(string forename, string surname, string email, string password)
        {
            /*
             Inserts new User into Database if User doesnt exist; returns true if succeeded
             IMPORTANT: Method takes unhashed password
             */
            Console.WriteLine("Run Method DBConnector::AddUser()...");

            string pwHash;

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            // 1. Check if User exists        
            query.CommandText = $"SELECT ID FROM user WHERE email = '{email}'";

            datareader = query.ExecuteReader();
            if (datareader.Read())
                // Email existiert schon in DB
                return false;
            datareader.Close();

            // 2. Insert User
            pwHash = HashConverter.ComputeSHA256Hash(password);
            query.CommandText = $@"INSERT INTO user(forename, surname, email, password)
                                  VALUES ('{forename}', '{surname}', '{email}', '{pwHash}')";

            query.ExecuteNonQuery();

            return true;
        }

        public List<string> GetUserData(string userID)
        {
            /*
             Returns a List with all user data for given userID
             */
            Console.WriteLine("Run Method DBConnector::GetUserByID()...");

            List<string> output = new List<string>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $"SELECT ID, forename, surname, email FROM user WHERE ID = {userID}";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                string ID = datareader.GetInt64(0).ToString();
                string forename = datareader.GetString(1);
                string surname = datareader.GetString(2);
                string email = datareader.GetString(3);

                output.Add(ID);
                output.Add(forename);
                output.Add(surname);
                output.Add(email);
            }

            return output;
        }

        public bool CheckCredentials(string email, string password)
        {
            /*
             Returns a bool value if the given login credentials are correct
             */
            Console.WriteLine("Run Method DBConnector::CheckCredentials()...");

            bool output = false;
            string pwHash = HashConverter.ComputeSHA256Hash(password);

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $"SELECT ID FROM user WHERE email = '{email}' AND password = '{pwHash}'";

            datareader = query.ExecuteReader();

            output = datareader.Read() ? true : false;

            return output;
        }

        public bool AddUserFavorite(string userID, string stockSymbol)
        {
            /*
             Inserts a new User Stock Favorite 
             */
            Console.WriteLine("Run Method DBConnector::AddUserFavorite()...");

            SQLiteCommand query = conn.CreateCommand();

            try
            {
                query.CommandText = $"INSERT INTO user_favorites(user_id, stock_sc) VALUE ({userID}, '{stockSymbol}')";
                query.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public List<string> GetUserFavorites(string userID)
        {
            /*
             Returns a List, comtaining all User Favorite stock symbols
             */
            Console.WriteLine("Run Method DBConnector::GetUserFavorites()...");

            List<string> output = new List<string>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = $"SELECT stock_sc FROM user_favorites WHERE user_id = {userID}";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {

                string symbol = datareader.GetString(0);
                output.Add(symbol);
            }

            return output;
        }

        public bool BuyStock(string userId, string symbol, double stockCourse, int quantity)
        {
            /*
             Inserts a new Transaction (Buy, 1) into Database
             */
            Console.WriteLine("Run Method DBConnector::BuyStock()...");

            SQLiteCommand query = conn.CreateCommand();

            try
            {
                query.CommandText = $@"INSERT INTO transactions(user_id, transaction_type_id, stock_sc, stock_course, quantity)
                                       VALUE ({userId}, 1, '{symbol}', {stockCourse}, {quantity})";
                query.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool SellStock(string userId, string symbol, double stockCourse, int quantity)
        {
            /*
             Inserts a new Transaction (Sell, 2) into Database
             */
            Console.WriteLine("Run Method DBConnector::SellStock()...");

            SQLiteCommand query = conn.CreateCommand();

            try
            {
                query.CommandText = $@"INSERT INTO transactions(user_id, transaction_type_id, stock_sc, stock_course, quantity)
                                       VALUE({userId}, 2, '{symbol}', {stockCourse}, {quantity})";
                query.ExecuteNonQuery();
            }
            catch
            {
                return false;
            }

            return true;
        }


        // Implementing Singleton pattern
        public static DBConnector Instance
        {
            get { return lazy.Value; }
        }

        private static readonly Lazy<DBConnector> lazy = new Lazy<DBConnector>(() => new DBConnector());

    }
}
