﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerTest.lib.DBConnector;
using ServerTest.lib.APIConnector;

namespace ServerTest
{
    public class BrokerService : IBrokerService
    {
        public DBConnector dbConnector { get; set; }
        public APIConnector apiConnector { get; set; }

        public BrokerService()
        {
              dbConnector = DBConnector.Instance;
        }

        //DB Connector Methoden
        public bool AddUser(string fname, string sname, string mail, string pw)
        {
            Console.WriteLine("User added");
             return dbConnector.AddUser(fname, sname, mail, pw);
        }

        public List<List<string>> GetAllUserData()
        {
            return dbConnector.GetAllUserData();
        }

        public List<string> GetUserData(string userID)
        {
            return dbConnector.GetUserData(userID);
        }

        public bool CheckCredentials(string email, string password)
        {
            return dbConnector.CheckCredentials(email, password);
        }

        public bool AddUserFavorite(string userID, string stockSymbol)
        {
            return dbConnector.AddUserFavorite(userID, stockSymbol);
        }

        public List<string> GetUserFavorites(string userID)
        {
            return dbConnector.GetUserFavorites(userID);
        }



        //API Connector Methoden
        public List<Dictionary<string, string>> SearchForKeyword(string keyword)
        {
            return apiConnector.SearchForKeyword(keyword);
        }

        public List<Dictionary<string, string>> GetChartData(string symbol)
        {
            return apiConnector.GetChartData(symbol);
        }

        public Dictionary<string, string> GetLatestCourse(string symbol)
        {
            return apiConnector.GetLatestCourse(symbol);
        }

        public bool BuyStock(string userId, string symbol, double stockCourse, int quantity)
        {
            return dbConnector.BuyStock(userId, symbol, stockCourse, quantity);
        }

        public bool SellStock(string userId, string symbol, double stockCourse, int quantity)
        {
            return dbConnector.SellStock(userId, symbol, stockCourse, quantity);
        }

        public List<string> GetLatestNews(string symbol)
        {
            return apiConnector.GetLatestNews(symbol);            
        }
    }
}
