﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Server;
using Server.lib.APIConnector;
using Server.lib.DBConnector;
using Server.lib.util;

namespace ServerGUI
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// Mainwindow fuer SERVER GUI mit allen WPF Elementen nur fuer den SERVER!
    /// Im Konstruktur wird die Methode "start" aufgerufen, worin der Server instanziiert und gestartet wird.
    /// </summary>
    public partial class MainWindow : Window
    {

        Server server;
        ServiceHost sh;

        /// <summary>
        /// Konstruktur Methode fuer Mainwindow XAML (WPF Server GUI)
        /// Darin wird start methode aufgerufen und alle Komponente (WPF Elemente) initialisiert
        /// </summary>

        public MainWindow()
        {
            Loaded += start;
            InitializeComponent();

        }

        /// <summary>
        /// Start Methode:
        /// Server wird instanziiert, Server GUI geupdated und Server verbindung sh hergestellt, 
        /// bei der die Verbindung durch die StartServer() Methode zurueckgegeben wird
        /// </summary>

        private void start(object sender, RoutedEventArgs e)
        {
            server = new Server();
            sh = server.StartServer();
            Console.WriteLine("Fenster geladen");
            updateGUI();
        }

        /// <summary>
        /// UpdateGUI Methode:
        /// Aktuallisiert die Server GUI und Benutzeroberflaeche der WPF Elemente des Mainwindows
        /// Aktuallisiert die API Calls
        /// Aktuallisiert die Informationen, welche angezeigt werden, wenn man einen Listeneintrag auswaehlt
        /// </summary>

        private void updateGUI()
        {
            UserListBox.Items.Clear();
            List<List<string>> userData = server.brokerService.GetAllUserData();
            foreach (List<string> user in userData)
            {
                string stri = "";
                for (int x = 0; x < user.Count; x++)
                {
                    stri += user[x] + " ";
                }
                UserListBox.Items.Add(stri);

            }
            apiCallsSync();
            updateSelectionList();

        }

        /// <summary>
        /// apiCallsSync() Methode:
        /// Hier werden die verbleibenden API Calls berechnet und ausgeben.
        /// Das Textlabel wird aktuallisiert.
        /// </summary>

        private void apiCallsSync()
        {
            List<int> remainingCalls = server.brokerService.GetRemainingCalls();
            APICallsLabel.Content = $"{remainingCalls[1]} / {remainingCalls[0]}";
            if (remainingCalls[1] <= 10)
            {
                APICallsLabel.Foreground = new SolidColorBrush(Colors.Red);

            }
            else if (remainingCalls[1] <= 50)
            {
                APICallsLabel.Foreground = new SolidColorBrush(Colors.Orange);

            }
            else
            {
                APICallsLabel.Foreground = new SolidColorBrush(Colors.Green);

            }
        }

        /// <summary>
        /// EinstellungenButton_Click() Methode:
        /// Wird aufgerufen wenn beim Mainwindow auf den EinstellungsButton gedrueckt wird.
        /// Erstellt das das EinstellungsWindow und Zeigt es an.
        /// Hinterher wird die Komplette GUI aktuallisiert (UpdateGUI() Methode wird aufgeruft)
        /// </summary>

        private void EinstellungenButton_Click(object sender, RoutedEventArgs e)
        {
            ServerEinstellungen serverEinstellungenWindow = new ServerEinstellungen(this);
            serverEinstellungenWindow.ShowDialog();
            updateGUI();
            updateSelectionList();
        }

        /// <summary>
        /// ServerStoppenButton_Click() Methode:
        /// Wird aufgerufen wenn beim Mainwindow auf den ServerStoppenButton gedrueckt wird.
        /// Stoppt den Server, Schließt die Verbindung.
        /// Abschließend wird das Fenster geschlossen
        /// </summary>

        private void ServerStoppenButton_Click(object sender, RoutedEventArgs e)
        {
            server.StoppServer(sh);
            Console.WriteLine("Server wird geschlossen.");
            this.Close();
        }

        /// <summary>
        /// DeleteButton_Click() Methode:
        /// Wird aufgerufen wenn beim Mainwindow auf den DeleteButton gedrueckt wird.
        /// User wird aus Datenbank und aus der UserListe geloescht.
        /// Abschließend wird die komplette GUI geupdated.
        /// </summary>

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            server.brokerService.RemoveUser(UserListBox.SelectedItem.ToString().Split(' ')[0]);
            UserListBox.Items.Remove(UserListBox.SelectedItem);
            updateGUI();
            updateSelectionList();
        }

        /// <summary>
        /// UserListBox_SelectionChanged() Methode:
        /// Wird aufgerufen wenn beim Mainwindow die UserSelection geändert wird.
        /// Ruft updateSelectionList() auf
        /// </summary>

        private void UserListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateSelectionList();
        }

        /// <summary>
        /// updateSelectionList() Methode:
        /// Wird aufgerufen wenn beim Mainwindow die UserSelection geändert wird.
        /// Aktuallisiert den Content der User Informations Elemente.
        /// Dazu gehoeren Benutzerinformationen, FavouritenListe und DepotInformationen.
        /// </summary>

        public void updateSelectionList()
        {
            if (UserListBox.SelectedItem != null)
            {
                string[] UserEintrag = UserListBox.SelectedItem.ToString().Split(' ');
                VornameTextBox.Text = UserEintrag[1];
                NachnameTextBox.Text = UserEintrag[2];
                EmailTextBox.Text = UserEintrag[3];
                //PasswortTextBox.Text = UserEintrag[4];
                FavouritenListBox.Items.Clear();
                List<Dictionary<string, string>> userFavourites = server.brokerService.GetUserFavorites(UserListBox.SelectedItem.ToString().Split(' ')[0]);
                foreach (Dictionary<string, string> favourites in userFavourites)
                {
                    FavouritenListBox.Items.Add(favourites["symbol"]);
                }
                DepotListBox.Items.Clear();
                List<Dictionary<string, string>> userDepot = server.brokerService.GetUserDepot(UserListBox.SelectedItem.ToString().Split(' ')[0]);
                foreach (Dictionary<string,string> dict in userDepot)
                {
                    DepotListBox.Items.Add($"{dict["quantity"]} x {dict["symbol"]}");
                    //Console.WriteLine(userDepot[x].ToString());
                }
            }
        }

        /// <summary>
        /// AddButton_Click() Methode:
        /// Wird aufgerufen wenn beim Mainwindow die AddButton gedrueckt wird.
        /// Vorher sollten in den Elementen entsprechende Informationen hinterlegt sein, die in die Datenbank hinzugefuegt werden sollen
        /// (Ein neuer User) Dazu gehoeren Benutzerinformationen.
        /// Dann wird ein neuer Nutzer registriert.
        /// Abschliessend wird die gesamte GUI aktuallisiert und danach die Selection und angezeigten Nutzerinformationen.
        /// </summary>

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string forename = VornameTextBox.Text;
            string surname = NachnameTextBox.Text;
            string email = EmailTextBox.Text;
            string password = PasswordPWBox.Password;
            

            server.brokerService.AddUser(forename, surname, email, password);
            UserListBox.Items.Add(forename + " " + surname + " " + email + " " + password);
            updateGUI();
            updateSelectionList();
        }

        /// <summary>
        /// RefreshButton_Click() Methode:
        /// Wird aufgerufen wenn beim Mainwindow die RefreshButton gedrueckt wird.
        /// Refresht alles.
        /// </summary>

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            updateGUI();
        }
    }
}
