﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Server;
using Server.lib.APIConnector;
using Server.lib.DBConnector;
using Server.lib.util;

namespace ServerGUI
{
   
    /// <summary>
    /// Interaktionslogik für ServerEinstellungen.xaml
    /// </summary>

    public partial class ServerEinstellungen : Window
    {

        /// <summary>
        /// Oeffentliche Variablen und Konstrukor:
        /// DBConnector wird deklariert
        /// Path APIKey.txt wird festgelegt
        /// 
        /// DBConnector wird instanziiert
        /// Komponente werden initialisiert
        /// APIKey.txt wird gelesen und zwischengespeichert
        /// </summary>

        private DBConnector dbConnector;
        string apikeyPath = @"..\..\..\Server\lib\APIConnector\APIKey.txt";

        public ServerEinstellungen(MainWindow mainWindow)
        {
            dbConnector = DBConnector.Instance;
            InitializeComponent();
            string line;
            string api_key;
            
            System.IO.StreamReader file =
            new System.IO.StreamReader(apikeyPath);
            while ((line = file.ReadLine()) != null)
            {
                api_key = line;
                Console.WriteLine(api_key);
                APIKeyTextBox.Text = api_key;
            }
            
            file.Close();
        }

        /// <summary>
        /// SpeichernButton_Click() Methode:
        /// Wird aufgerufen wenn auf den SpeichernButton gedrueckt wird.
        /// Liest APIKey und schreibt ggf. neu.
        /// DER SERVER MUSS HINTERHEU NEUGESTARTET WERDEN.****
        /// Schliesst zuletzt 
        /// </summary>

        private void SpeichernButton_Click(object sender, RoutedEventArgs e)
        {
            string api_key;
            api_key = APIKeyTextBox.Text;
            System.IO.File.WriteAllText(apikeyPath, api_key);
            MessageBox.Show("Einstellungen wurden gespeichert. Sie können den Server nun Neustarten");
            this.Close();
            
        }

        /// <summary>
        /// AbbrechenButton_Click() Methode:
        /// Wird aufgerufen wenn auf den AbbrechenButton gedrueckt wird.
        /// Fenster wird geschlossen
        /// </summary>

        private void AbbrechenButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// InitDBButton_Click() Methode:
        /// Wird aufgerufen wenn auf den InitDBButton gedrueckt wird.
        /// Datenbank wird neu intialisiert.
        /// </summary>

        private void InitDBButton_Click(object sender, RoutedEventArgs e)
        {
            dbConnector.InitializeDB();
            MessageBox.Show("Datenbank neu intitialisiert", "Hinweis" , MessageBoxButton.OK);
        }

        
    }
}
