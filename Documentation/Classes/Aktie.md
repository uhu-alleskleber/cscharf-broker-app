Info:
An object blueprint for creating instances of a stock - "Aktie".
- public
- Author: sk

Variables:

- double Course
- string Name
- string Abbreviation
- int Anzahl
- List<string> TimeStamps
- List<double> ChartValues
- string[] News
- string Boersenname
- double Change
- string Currency

Constructors:

- public Aktie() {}
- public Aktie(double course, String name, String abbreviation) {}
- public Aktie(double course, String name, String abbreviation, int anzahl) {}

Methods:
no methods