# Server.lib.DBConnector.DBConnector


> *class* **Server.lib.DBConnector.DBConnector**()

This class creates or open a Database and establish a connection. The methods must be used to interact with the database.
<br><br>

---
> dbName

Name of the Database<br>
**Type:** `string` 
<br><br>

---
> conn

Connection Instance of the Database<br>
**Type:** `System.Data.SQLite.SQLiteConnection` 
<br><br>

---
> CreateConnection()

Reads or creates a database and returns the connection attribute<br>
**Returns:**  `System.Data.SQLite.SQLiteConnection` 
<br><br>

---
> InitializeDB()

Reinitialize Database from sql-script
<br><br>

---
> GetAllUserData()

Returns a List containing a List with Userdata. <br>
Structure:
```json
[
    [
        ID,
        forename,
        surname,
        email,
        balance
    ],
    [
        ID,
        forename,
        surname,
        email,
        balance
    ],
]
```
**Returns:**  `List<List<string>>` 
<br><br>

---
> AddUser(string forename, string surname, string email, string password)

Adds a user to the database <br>
**Returns:**  `bool` 
<br><br>

---
> RemoveUser(string userId)

Removes User with userId From Database <br>
**Returns:**  `bool` 
<br><br>

---
> GetUserData(string userId)

Gets User Data just like `GetAllUSerData()` but for one user<br>
Structure:
```json
[
    ID,
    forename,
    surename,
    email,
    balance
]
```
**Returns:**  `List<string>` 
<br><br>

---
> CheckCredentials(string email, string password)

Checks if the Email-Password-Comibination is correct (DB-Check). Returns ID if correct otherwise 0.<br>
**Returns:**  `int` 
<br><br>

---
> AddUserFavorite(string userID, string stockSymbol, string stockName, string exchange)

Adds a User-Stock-Favorite to the DB<br>
**Returns:**  `bool` 
<br><br>

---
> GetUserFavorites(string userID)

Gets All User-Stock-Favorites.<br>
Structure:
```json
[
    {
        "symbol": symbol,
        "name": name,
        "exchange": exchange
    },
    {
        "symbol": symbol,
        "name": name,
        "exchange": exchange
    },
    ...
]
```
**Returns:**  `List<Dictionary<string, string>>`
<br><br>

---
> BuyStock(string userId, string symbol, double stockCourse, int quantity)

Adds new Buy-Transaction to the Database. The Course must be checked before by `Server.lib.ApiConnector.APIConnector`.<br>
**Returns:**  `bool` 
<br><br>

---
> SellStock(string userId, string symbol, double stockCourse, int quantity)

Adds new Sell-Transaction to the Database. The Course must be checked before by `Server.lib.ApiConnector.APIConnector`.<br>
**Returns:**  `bool` 
<br><br>

---
> GetUserDepot(string userId)

Calculates the User-Depot by substracting buyed and selled stocks.<br>
Structure:
```json
[
    {
        "symbol": symbol,
        "quantity": quantity
    },
    {
        "symbol": symbol,
        "quantity": quantity
    },
    ...
]
```
**Returns:**  `List<Dictionary<string, string>>` 

# Database Structure

```mermaid
classDiagram
user <|-- user_favorites
user <|-- transactions
transactions <|-- tranaction_types

user : ID INTEGER PK
user : forename TEXT
user : surname TEXT
user : email TEXT
user : password TEXT
user : balance DOUBLE
user_favorites : ID INTEGER PK
user_favorites : user_id INTEGER FK
user_favorites : stock_sc TEXT
tranaction_types : ID INTEGER PK
tranaction_types : type_name TEXT
transactions : ID INTEGER PK
transactions : user_id INTEGER FK
transactions : transaction_type_id INTEGER FK
transactions : stock_sc TEXT
transactions : stock_course DOUBLE
transactions : quantity INTEGER
```



