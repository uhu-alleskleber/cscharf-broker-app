Info:
A class for the TransactionHandler - all the methods the GUI will need to get info from or send to the DBConnector or APIConnector.
Author: sk

Variables: no variables

Constructor: no constructor

Methods:

- bool checkServerConn() //tbd
- bool CheckCredentials(string email, string password)
    returns true, if CheckCredentials() from DBConnector returns true
- double getUserBalance(Person user)
    updates and returns current user.Balance
- Person getUser(string email, string password)
    returns the Person-object that corresponds with said email and password, all user data is up to date
- 

