Info:
A blueprint class for creating instances of users - "Person".
- public
- Author: sk

Variables:

- public List<Aktie> Depot
- public List<Aktie> Favorites
- public string Id
- public string Forename
- public string Surname
- public string Email
- public string Password //To be deleted
- public double Balance

Constructor:

- public Person(string id, string forename, string surname, string email, string password, double balance)
- public Person(){}

Methods:

- public bool buy(Aktie stonk, int quantity)
    Returns: True if Buy-Interaction from the DBConnector was successful
- public bool fav(Aktie stonk, int quantity)
    Returns: True if Fav-Interaction from the DBConnector was successful
- // public bool sell(Aktie stonk, int quantity)
    Returns: True if Sell-Interaction from the DBConnector was successful

All the named methods send out a method naming to the BrokerService-Connection, so actually the methods just get called in the DBConnector.