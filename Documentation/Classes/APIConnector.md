# Server.lib.APIConnector.APIConnector


> *class* **Server.lib.APIConnector.APIConnector**()

This class is used to have a uniform interface for calling webservicecs. Responses will be returned in C#-friendly Datatypes
<br><br>

---
> rapidApiHost

API-Header Property<br>
**Type:** `string` 
<br><br>

---
> rapidApiKey

API-Header Key TODO: Read from File<br>
**Type:** `string` 
<br><br>

---
> request

**Type:** `RestSharp.RestRequest` 
<br><br>

---
> SearchForKeyword(string keyword)

Returns a List of found StockSymbols with extra data for given keyword<br>
Structure:
```json
[
    {
        'symbol': symbol,
        'name': name (shortname),
        'excange': exchange,
        'score': score (?)
    },
    ...
]
```
**Returns:**  `List<Dictionary<string, string>>` 
<br><br>

---
> GetChartData(string symbol)

Returns a List with the Chartdata from the last 5 days in 5 minute intervals.<br>
Structure:
```json
 [
    {
        'currency': currency,
        'timestamp': timestamp,
        'course': course
    },
    ...
 ]
```
**Returns:** `List<Dictionary<string, string>>`
<br><br>

---
> GetLatestCourse(string symbol)

Returns Latest Course and Change rate of stock symbol <br>
Structure:
```json
{
    'currency': currency,
    'course': course,
    'change': change
}
```
**Returns:**  `Dictionary<string, string>` 
<br><br>

---
> GetLatestNews(string symbol)

Returns The latest news for the stock to show in an overview window <br>
Structure:
```json
[
    title,
    link,
    content,
    timestamp
]
```
**Returns:**  `List<string>` 
<br><br>