﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLogic
{
    public class Person
    {
        //Singleton machen
        BrokerService.BrokerServiceClient service = new BrokerService.BrokerServiceClient();
        public List<Aktie> Depot { get; set; }
        public List<Aktie> Favorites { get; set; }

        public string Id { get; set; }

        public string Forename { get; set; }
        public string Surname { get; set; }
        
        public string Email { get; set; }
        public string Password { get; set; }

        public double Balance { get; set; }
        
        public Person() { }
        public Person(string id, string forename, string surname, string email, double balance)
        {
            Id = id;
            Forename = forename;
            Surname = surname;
            Email = email;
            Balance = balance;
        }     

        //Methoden
        public bool buy(Aktie stonk, int quantity)
        {
            //Test auf Guthaben im Transaktionshandler
            string course = Convert.ToString(stonk.Course);
            course = course.Replace(",", ".");
   
            bool erg = service.BuyStock(this.Id, stonk.Abbreviation, course, quantity);

            return erg;
        }

        //public bool fav(Aktie stonk)
        //{
        //    //Test auf ob bereits in Fav enthalten
        //    bool erg = service.AddUserFavorite(user.Id, stonk.Abbreviation);

        //    return erg;
        //}

        public bool sell(Aktie stonk, int quantity)
        {
            //Test auf Depot etc im TransaktionsHandler
            string course = Convert.ToString(stonk.Course);
            course = course.Replace(",", ".");
            bool erg = service.SellStock(this.Id, stonk.Abbreviation, course, quantity);

            return erg;
        }

        //public bool unfav(Aktie stonk)
        //{
        //    //Test auf ob bereits in Fav enthalten
        //    bool erg = service.DeleteUserFavorite(this.Id, stonk.Abbreviation); //noch nicht geschrieben auf DB-Seite

        //    return erg;
        //}

    }
}
