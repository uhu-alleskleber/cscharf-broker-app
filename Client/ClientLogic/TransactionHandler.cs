﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ClientLogic.BrokerService;

namespace ClientLogic
{
    public class TransactionHandler
    {
        //singleton machen
        IBrokerService service = new BrokerServiceClient();

        //TODO: Methode zum Check der Verbindung mit dem Server, asynch, Weitergabe des Status an GUI, async?
        //public TransactionHandler()
        //{

        //}

        async public void checkServerConn()
        {

        }
                
        public bool CheckCredentials(string email, string password)
        {
            int ergebnis = service.CheckCredentials(email, password);

            if (ergebnis == 0) // == 0
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public double getUserBalance(Person user)
        {
            string[] currUserDataList = service.GetUserData(user.Id);
            user.Balance = Convert.ToDouble(currUserDataList[4]);
            return user.Balance;
        }
        //gibt PersonObjekt zurueck mit seinen Infos
        public Person getUser(string email, string password)
        {
            int ergebnis = service.CheckCredentials(email, password); //erg: 0 = nobody; !0 = userId

            Person currentUser = new Person("0", "nobody", "nobody", "nobody", 0.0); //leere Person, fuer Testzwecke befuellt
            //falls irgendwo "nobody" ausgegeben wird, ist was schiefgelaufen in dieser methode
            if (ergebnis == 0)
            {
                return currentUser;
            }
            else
            {
                //ergebnis ist userId als int
                //userIdString ist userId als string              

                string userIdString = Convert.ToString(ergebnis);

                string[] currUserDataList = service.GetUserData(userIdString); //hier obwohl ausgabe List<string>, eigentlich ausgabe string[]

                currentUser.Id = currUserDataList[0];
                currentUser.Forename = currUserDataList[1];
                currentUser.Surname = currUserDataList[2];
                currentUser.Email = currUserDataList[3];
                currentUser.Balance = Convert.ToDouble(currUserDataList[4]);
                

                currentUser.Depot = getUserDepot(currentUser);
                currentUser.Favorites = getUserFavorites(currentUser);

            }

            return currentUser;
        }

        public Person updateUserInfo(Person user)
        {
            string[] currUserDataList = service.GetUserData(user.Id); //hier obwohl ausgabe List<string>, eigentlich ausgabe string[]

            user.Id = currUserDataList[0];
            user.Forename = currUserDataList[1];
            user.Surname = currUserDataList[2];
            user.Email = currUserDataList[3];
            user.Balance = Convert.ToDouble(currUserDataList[4]);


            user.Depot = getUserDepot(user);
            user.Favorites = getUserFavorites(user);

            return user;
        }

        public List<Aktie> getUserDepot(Person user)
        {
            Dictionary<string, string>[] currUserDepotList = service.GetUserDepot(user.Id);
            user.Depot = new List<Aktie>();
            foreach (Dictionary<string, string> entry in currUserDepotList)
            {
                Aktie newStonk = new Aktie();
                newStonk.Abbreviation = entry["symbol"];
                newStonk.Anzahl = Convert.ToInt16(entry["quantity"]);
                //nach dem Namen suchen

                Dictionary<string, string>[] getName = service.SearchForKeyword(entry["symbol"]);
                foreach(Dictionary<string, string> entryName in getName)
                {
                    newStonk.Name = entryName["name"];
                }
                newStonk.Course = GetLatestCourse(newStonk);
                user.Depot.Add(newStonk);
                //also im Depot sind jetzt die Aktien mit Abbreviaton und anzahl
                //weitere Infos sollten folgen
            }
            //Mittels Kürzel den Namen bekommen

            return user.Depot;
        }

        public List<Aktie> getUserFavorites(Person user)
        {
            List<Aktie> UserFavorites = new List<Aktie>();

            Dictionary<string, string>[] currUserFavList = service.GetUserFavorites(user.Id);

            foreach (Dictionary<string, string> dict in currUserFavList)
            {
                Aktie newStonk = new Aktie();
                newStonk.Abbreviation = dict["symbol"];
                newStonk.Name = dict["name"];
                newStonk.Boersenname = dict["exchange"];
                //newStonk = newStonk.getNewInfo();                              
                newStonk.Course = GetLatestCourse(newStonk);
                UserFavorites.Add(newStonk);       
                }
            return UserFavorites;
        }

        //TBD
        public void getChartData(Aktie stonk) //Aufrufen bei Klick von Fav in Liste, da wird ChartData geladen
        {
            stonk.TimeStamps = new List<string>();
            stonk.ChartValues = new List<double>();
            Dictionary<string, string>[] chartData = service.GetChartData(stonk.Abbreviation);

            foreach (Dictionary<string, string> dict in chartData)
            {
                stonk.Currency = dict["currency"];
                stonk.TimeStamps.Add(dict["timestamp"]);
                stonk.ChartValues.Add(Convert.ToDouble(dict["course"]));                
            }
        }        

        public double GetLatestCourse(Aktie stonk)
        {
            Dictionary<string, string> dict = service.GetLatestCourse(stonk.Abbreviation);
            stonk.Currency = dict["currency"];
            stonk.Course = Convert.ToDouble(dict["course"]);
            stonk.Change = Convert.ToDouble(dict["change"]);
            return stonk.Course;
        }

        public Aktie getNewInfo(Aktie stonk)
        {
            stonk.Course = GetLatestCourse(stonk);
            stonk.News = getNews(stonk); //nach dem merge mit pf klappt
            getChartData(stonk);

            return stonk;
        }

        public string[] getNews(Aktie stonk)
        {
            stonk.News = service.GetLatestNews(stonk.Abbreviation);

            return stonk.News;
        }

        public bool registerUser(string forename, string surname, string email, string password)
        {
            //check ob email bereits registriert  
            if (isAlreadyRegistered(email))
            {
                return false;
            }
            else
            {
                bool erg = service.AddUser(forename, surname, email, password); //hier kommt wsl true an
                return erg;
            }
        }

        public bool isAlreadyRegistered(string email)
        {
            string[][] allUserData = service.GetAllUserData();

            foreach (string[] listString in allUserData)
            {
                if (email == listString[3])
                {
                    return true;
                }
            }
            return false;
        }

        //public bool deleteAccount(Person user)
        //{
        //    int erg = service.CheckCredentials(user.Email, user.Password);

        //    string userId = user.Id;

        //    if (erg != 0)
        //    {
        //        //bool temp = service.DeleteUser(userId); //noch nicht vorhanden
        //        return true; //return temp//wahrscheinlich true

        //    } else
        //    {
        //        return false;
        //    }            
        //}

        public bool buyStonk(Person user, Aktie stonk, int quantity) //async machen später
        {
            //TBD: Get Update/Newest Stock Course and Balance
            user.Depot = getUserDepot(user);
            if(user.Balance >= (stonk.Course * quantity))
            {
                bool erg = user.buy(stonk, quantity);
                user.Depot = getUserDepot(user); //hier async/ await - warten auf antwort und dann kauf, wenn nicht dann abbruch
                if (erg)
                {
                    user.Depot = getUserDepot(user);
                    user.Balance = getUserBalance(user);
                }
                return erg;
            } else
            {
                return false;
            }            
        }

        public bool favStonk(Person user, Aktie stonk)
        {
            //TBD: Check ob bereits in Favs enthalten
            //uebergebe folgende infos an service: abbreviation, name, boerse; TBD IN GUI
            bool erg = service.AddUserFavorite(user.Id, stonk.Abbreviation, stonk.Name, stonk.Boersenname);
            user.Favorites = getUserFavorites(user);

            //user id, symbol und exchange wird gebraucht fuer adduserfavorite
            return erg;
        }

        public bool sellStonk(Person user, Aktie stonk, int quantity)
        {
            //return: 0 - der User will mehr verkaufen als er hat (er hat die aktie, aber nicht diese anzahl)
            //return: 1 - erfolgreich verkauft
            //return: 2 - versucht zu verkaufen, aber DBConnector Methode sell liefert false
            //return: 3 - user hat die aktie nicht im depot
            foreach(Aktie depotEntry in user.Depot)
            {
                if (depotEntry.Abbreviation == stonk.Abbreviation)
                {
                    if(depotEntry.Anzahl < quantity)
                    {                        
                        return false;
                    } else
                    {
                        bool erg = user.sell(stonk, quantity);
                        user.Depot = getUserDepot(user);
                        if (erg)
                        {
                            user.Depot = getUserDepot(user);
                            user.Balance = getUserBalance(user);
                            return true;
                        }                            
                        else
                            return false;
                    }
                }                                  
            }   
            return false;  
        }

        //public bool unfav(Person user, Aktie stonk)
        //{
        //    //TBD: Check ob in Favs enthalten
        //    bool erg = user.unfav(stonk);
        //    user.Favorites = getNewUserFavs(user);
        //    return erg;
        //}

        //    //otto will eine methode, die aktieninfos abruft bei kuerzelaufruf
        //public Aktie getStock(string abbreviation)
        //{


        //    return stonk;
        //}

            //liefert liste von aktien, die nur mit kuerzel, name und boersenname bemannt sind
        public List<Aktie> SearchKeywordErgebnisse(string suchString)
        {
            Dictionary<string, string>[] searchResult = service.SearchForKeyword(suchString);
            List<Aktie> result = new List<Aktie>();

            
            
            foreach (Dictionary<string, string> dict in searchResult)
            {
                Aktie newStonk = new Aktie();
                newStonk.Abbreviation = dict["symbol"];
                newStonk.Name = dict["name"];
                newStonk.Boersenname = dict["exchange"];
                result.Add(newStonk);
            }
            return result;
        }


    }
}
