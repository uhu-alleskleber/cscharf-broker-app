﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLogic
{
    public class Aktie
    {
        //Diagrammdarstellung: Kurshistorie (Liste/Array mit zeitstempel und kurs)
        BrokerService.BrokerServiceClient service = new BrokerService.BrokerServiceClient();

        public double Course { get; set; }

        public String Name { get; set; } // ganzer name, z.B. Siemens AG
        public String Abbreviation { get; set; } //Kuerzel von Aktie, z.b. SIE.DE
        
        public int Anzahl { get; set; } //wichtig fuer Depot-Anzahl Angabe

        public List<double> ChartValues { get; set; }
        public List<string> TimeStamps { get; set; }

        public string[] News { get; set; }
        
        public string Boersenname { get; set; }
        public double Change { get; set; } //aenderung von vorherigem wert

        public string Currency { get; set; }

        public Aktie(){ }
        

        public Aktie(double course, String name, String abbreviation)
        {
            Course = course;
            Name = name;
            Abbreviation = abbreviation;
        }
        public Aktie(double course, String name, String abbreviation, int anzahl)
        {
            Course = course;
            Name = name;
            Abbreviation = abbreviation;
            Anzahl = anzahl;
        }        

        //bekomme fuer kurs liste von double werten
        //gui kriegt sie dann genau so
                           
        
        //gibt die aktuellste Infos des Aktien-Objektes inklusive Kurs, News und Chart aka. Kurshistorie mit Zeitstempeln
        
    }
}
