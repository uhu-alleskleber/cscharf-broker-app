﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ClientLogic.BrokerService;

namespace ClientLogic
{
    class Program
    {
        //folgendes kann vielleicht weg, weil verbindung zwischen clientlogic und server bereits in "connected services" vorhanden:
        const string hostName = "net.tcp://localhost";
        const int serverPort = 1337; //vorerst mit int, am ende zum string umschalten
        const string serviceName = "BrokerApplication";

        IBrokerService service = new BrokerServiceClient();


        static void Main(string[] args)
        {
             
            
            Console.WriteLine("Client started. Testing:");
            //hier ist Testgelaende


            IBrokerService service = new BrokerServiceClient();

            TransactionHandler th1 = new TransactionHandler();
            Person test1 = th1.getUser("pascal.fabiano@oython.fangirl", "pfabiano");
            List<Aktie> favs = test1.Favorites;

            string testname = test1.Forename;
            string testnachname = test1.Surname;
            string email = test1.Email;
            Console.WriteLine(testname + testnachname + email);

            List<Aktie> vonTH = th1.getUserFavorites(test1); //ist leer
            
            Dictionary<string, string>[] vonDBC = service.GetUserFavorites(test1.Id);

            string ausgabe1 = "Aktien:";
            Console.WriteLine(ausgabe1);
            
            Console.WriteLine(vonDBC);
            Console.WriteLine(vonDBC.Count());

            foreach (Dictionary<string,string> dict in vonDBC)
            {
                Console.WriteLine(dict);
                foreach (KeyValuePair<string, string> pair in dict)
                {
                    Aktie newStonk = new Aktie();
                    if (pair.Key == "symbol")
                    {
                        newStonk.Abbreviation = pair.Value;
                        Console.WriteLine("Abb:" + newStonk.Abbreviation);
                    }
                    if (pair.Key == "name")
                    {
                        newStonk.Name = pair.Value;
                        Console.WriteLine("Name:" + newStonk.Name);
                    }
                    if (pair.Key == "exchange")
                    {
                        newStonk.Boersenname = pair.Value;
                        Console.WriteLine("BName:" + newStonk.Boersenname);
                    }                    
                }                
                
            }

            string ausgabeEnde = "Ende Ausgabe";
            Console.WriteLine(ausgabeEnde);

            Console.WriteLine();
            

            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();         
        }       


    }
}
