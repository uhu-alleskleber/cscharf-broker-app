﻿using Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ClientLogic;

namespace Client
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    /// 
   
    public partial class App : Application, INotifyPropertyChanged
    {
        private bool loggedIn;
        public bool isLoggedIn
        {
            get {
                return loggedIn;
            }
            set
            {
                loggedIn = value;
                OnPropertyChanged(nameof(isLoggedIn));
            }
        }
        public Person Person { get; set; }

        public TransactionHandler th { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        //Ganzen Daten und Methoden für die Applikation
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int refreshFreshhold
        {
            //Auf Zeitpunkt
            get
            {
                return 3;
            }
        }
    }
    
}
