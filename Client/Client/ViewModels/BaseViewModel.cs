﻿using Client.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientLogic;

namespace Client.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        private int heightElement = 23;
        private bool loggedIn;
        public int HeightElement
        {
            get
            {
                return heightElement;
            }
        }

        public bool LoggedIn
        {
            get
            {
                return loggedIn;
            }
            set
            {
                loggedIn = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Aktie selectedAktie
        {
            get;
            set;
        }
    }
}
