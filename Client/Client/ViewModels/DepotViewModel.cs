﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using LiveCharts;
using LiveCharts.Wpf;

namespace Client.ViewModels
{
    public partial class DepotViewModel : BaseViewModel
    {
        public LiveCharts.SeriesCollection SampleData { get; set; }
        public DepotViewModel()
        {
            SampleData = new LiveCharts.SeriesCollection
      {
        new LineSeries
        {
          Values = new ChartValues<double> {}},
        };
        }
    }
}
        
