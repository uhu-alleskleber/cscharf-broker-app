﻿using Client.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.ViewModels
{

    public partial class MainViewModel : BaseViewModel
    {
        // allegemeine App Kontrolliert Navigation zwischen View Models
        //MainViewModel legt fest was dargestellt wird
        private BaseViewModel selectedViewModel;
        public BaseViewModel SelectedViewModel
        {
            get { return selectedViewModel; }
            set {
                selectedViewModel = value;
                OnPropertyChanged(nameof(SelectedViewModel));
            }
        }
        //update View Command
        public ICommand UpdateViewCommand { get; set; }
        public MainViewModel()
        {
            if (selectedViewModel == null)
            {
                //selectedViewModel = new LoginViewModel();
                //selectedViewModel = new RegisterViewModel();
            }
            UpdateViewCommand = new UpdateViewCommand(this);
        }
    
    }
}
