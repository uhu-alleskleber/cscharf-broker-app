﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Client.Views;
using Client.ViewModels;
using System.ComponentModel;
using System.Windows.Threading;

namespace Client
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public bool isVisible_LoggedIn {
            get
            {
                //OnPropertyChanged("(Application.Current as App).isLoggedIn");
                //return !(Application.Current as App).isLoggedIn;
                return false;
                
            }
            set
            {
                //value = !(Application.Current as App).isLoggedIn;
                //OnPropertyChanged("(Application.Current as App).isLoggedIn");
                value = false;
            }
        }
        //Event erzeugen dass darauf Reagiert wenn sich an der App was ändert
        public MainWindow()
        {
           
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds((Application.Current as App).refreshFreshhold);
            timer.Tick += checkComponents;
            timer.Start();
            InitializeComponent();
            if ((Application.Current as App).isLoggedIn)
            {
                //Picture.Children.Clear();
                LogOut.Visibility = Visibility.Visible;
                LoginBtn.Visibility = Visibility.Collapsed;
                DashboardBtn.Visibility = Visibility.Visible;
                KaufenBtn.Visibility = Visibility.Visible;
                suchenBtn.Visibility = Visibility.Visible;
            }
            
            else
            {
                LogOut.Visibility = Visibility.Collapsed;
                LoginBtn.Visibility = Visibility.Visible;
                DashboardBtn.Visibility = Visibility.Collapsed;
                KaufenBtn.Visibility = Visibility.Collapsed;
                suchenBtn.Visibility = Visibility.Collapsed;
            }
            
            DataContext = new MainViewModel();

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            //SelectedViewModel 
            
        }

        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current as App).isLoggedIn = false;
            (Application.Current as App).Person = null;
            
        }

        private void checkComponents(object sender, EventArgs e)
        {
            if((Application.Current as App).isLoggedIn==false)
            {
                LogOut.Visibility = Visibility.Collapsed;
                LoginBtn.Visibility = Visibility.Visible;
                DashboardBtn.Visibility = Visibility.Collapsed;
                KaufenBtn.Visibility = Visibility.Collapsed;
                suchenBtn.Visibility = Visibility.Collapsed;
                InitializeComponent();
            }
            else
            {
                LogOut.Visibility = Visibility.Visible;
                LoginBtn.Visibility = Visibility.Collapsed;
                DashboardBtn.Visibility = Visibility.Visible;
                KaufenBtn.Visibility = Visibility.Visible;
                suchenBtn.Visibility = Visibility.Visible;
                InitializeComponent();
            }
        }
    }
}
