﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Client.ViewModels;

namespace Client.Commands
{
    public class UpdateViewCommand : ICommand
    {
        private MainViewModel viewModel;

        public UpdateViewCommand(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }
        MainViewModel ViewModels { get; set; }
        public void Execute(object parameter)
        {
            Console.WriteLine("Update View Command was called");
            if(parameter.ToString() == "Register")
            {
                viewModel.SelectedViewModel = new RegisterViewModel();
            }
            if (parameter.ToString() == "Login")
            {
                viewModel.SelectedViewModel = new LoginViewModel();
            }
            if (parameter.ToString() == "Logout")
            {
                (Application.Current as App).isLoggedIn = false;
                (Application.Current as App).Person = null;
                MainWindow mw = (Application.Current as App).Windows.OfType<MainWindow>().FirstOrDefault();
                if (mw != null)
                {
                    ImageBrush b1 = new ImageBrush();
                    b1.ImageSource = new BitmapImage(new Uri(@"../../Images/LoginRegister.png", UriKind.RelativeOrAbsolute));
                    mw.Background = b1;

                }

                viewModel.SelectedViewModel = new LoginViewModel();
            }
            if (parameter.ToString() == "Favoriten")
            {
                MainWindow mw = (Application.Current as App).Windows.OfType<MainWindow>().FirstOrDefault();
                if (mw != null)
                {
                    ImageBrush b1 = new ImageBrush();
                    b1.ImageSource = new BitmapImage(new Uri(@"../../Images/Favoriten.png",UriKind.RelativeOrAbsolute));
                    mw.Background = b1;

                }
                viewModel.SelectedViewModel = new FavoritenViewModel();
            }
            if (parameter.ToString() == "Kaufen")
            {
                viewModel.SelectedViewModel = new KaufenViewModel();
            }
            if (parameter.ToString() == "Depot")
            {
                MainWindow mw = (Application.Current as App).Windows.OfType<MainWindow>().FirstOrDefault();
                if (mw != null)
                {
                    ImageBrush b1 = new ImageBrush();
                    b1.ImageSource = new BitmapImage(new Uri(@"../../Images/Depot.png", UriKind.RelativeOrAbsolute));
                    mw.Background = b1;

                }
                viewModel.SelectedViewModel = new DepotViewModel();
            }
            if (parameter.ToString() == "Verkaufen")
            {
                viewModel.SelectedViewModel = new VerkaufenViewModel();
            }
            if(parameter.ToString() == "Suchen")
            {
                
                MainWindow mw = (Application.Current as App).Windows.OfType<MainWindow>().FirstOrDefault();
                if (mw != null)
                {
                    ImageBrush b1 = new ImageBrush();
                    b1.ImageSource = new BitmapImage(new Uri(@"../../Images/Suche.png", UriKind.RelativeOrAbsolute));
                    mw.Background = b1;

                }
                viewModel.SelectedViewModel = new SuchenViewModel();
            }
            if (parameter.ToString() == "Home")
            {

            }
        }
    }
}
