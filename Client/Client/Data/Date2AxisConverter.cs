﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Client.Data
{
    public class Date2AxisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime && targetType == typeof(double))
            {
                return ((DateTime)value).Ticks / 10.0;
                // See constructor of class Microsoft.Research.DynamicDataDisplay.Charts.DateTimeAxis
                // File: DynamicDataDisplay.Charts.Axes.DateTime.DateTimeAxis.cs

                // alternatively, see the internal class Microsoft.Research.DynamicDataDisplay.Charts.DateTimeToDoubleConversion

            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
