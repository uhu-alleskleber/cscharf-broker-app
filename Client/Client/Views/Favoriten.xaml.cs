﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLogic;
using System.Windows.Forms;
using Client.Views;
//using System.Windows.Forms.DataVisualization.Charting;
using System.Collections.Specialized;
using System.Windows.Threading;
using Client.ViewModels;
using LiveCharts.Wpf;
using LiveCharts;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Dashboard.xaml
    /// </summary>
    public partial class Favoriten : System.Windows.Controls.UserControl
    {
        private int heightElement = 23;
        System.Windows.Controls.Label header;
        //Wenn es geht Aktieninfo als ListView
        System.Windows.Controls.DataGrid AktienListe;
        System.Windows.Controls.TextBlock AktienInfo;
        System.Windows.Controls.TextBlock SelectedAktieName;
        System.Windows.Controls.Label BalanceLbl;
        System.Windows.Controls.Label Balance;
        int selectedIndexOld=-2;
        public Aktie selectedAktie;
        System.Windows.Controls.Button kaufenSpeziell;
        System.Windows.Controls.WebBrowser newsWeb;

        private Line xAxisLine, yAxisLine;
        private double xAxisStart = 100, yAxisStart = 100, interval = 100;
        private Polyline chartPolyline;

        private Point origin;
        private List<Holder> holders;
        private List<Value> values;

        public Favoriten()
        {
            
            InitializeComponent();
            buildPageFct();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds((System.Windows.Application.Current as App).refreshFreshhold);
            //timer.Tick += buildPage;
            timer.Start();
            
        }
        private DataTable creatingDataGrid()
        {
            DataTable dt = new DataTable();
            DataColumn AktienName = new DataColumn("Name", typeof(string));
            DataColumn AktienKürzel = new DataColumn("Kürzel", typeof(string));
            DataColumn Kurs = new DataColumn("Kurs", typeof(double));

            dt.Columns.Add(AktienName);
            dt.Columns.Add(AktienKürzel);
            dt.Columns.Add(Kurs);
            return dt;
        }
        private void selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AktienInfo.Text = "";
            SelectedAktieName.Text = "";
            System.Windows.Controls.DataGrid dG = sender as System.Windows.Controls.DataGrid;
            if (dG.SelectedItem == null)
            {
                kaufenSpeziell.Visibility = Visibility.Collapsed;
            }
            //else if (selectedIndexOld == lb.SelectedIndex)
            //{
            //    kaufenSpeziell.Visibility = Visibility.Collapsed;
            //    lb.SelectedIndex = -1;
            //    SelectedAktieName.Content = "";
            //    selectedIndexOld = -2;
            //    //nur Kaufen button anzeigen --> neues Pop Up
            //    //Event für Button

            //    Grid.SetRow(kaufenSpeziell, 1);
            //    Grid.SetColumn(kaufenSpeziell, 1);
            //    //Picture.Children.Add(verkaufen);
            //    //Picture.Children.Add();

            //}
            else
            {
                //Aktie Darstellen

                
                selectedIndexOld = dG.SelectedIndex;
                DataRowView d1 = (DataRowView)dG.SelectedItem;
                
                selectedAktie = getSelectedAKtie(d1.Row.ItemArray[1].ToString());
                //Aktieninfo als Grid / View darstellen
                passViewZurAktieAn();

                //Kaufen/ Verkaufen Button anzeigen
                kaufenSpeziell = new System.Windows.Controls.Button();
                kaufenSpeziell.Click += new RoutedEventHandler(kaufSpeziell);
                //Event für Button
                Thickness margin = kaufenSpeziell.Margin;
                margin.Left = 10;
                kaufenSpeziell.Margin = margin;
                kaufenSpeziell.VerticalAlignment = VerticalAlignment.Bottom;
                kaufenSpeziell.VerticalContentAlignment = VerticalAlignment.Center;
                kaufenSpeziell.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                kaufenSpeziell.Content = "Ausgewählte Aktien kaufen";
                kaufenSpeziell.Height = heightElement;


                Grid.SetRow(kaufenSpeziell, 12);
                Grid.SetColumn(kaufenSpeziell, 3);
                Grid.SetColumnSpan(kaufenSpeziell, 4);
                Picture.Children.Add(kaufenSpeziell);
            }
            //lb.SelectedIndex = -1;

        }

        private void fillingDataGrid(DataTable dt)
        {
            //Kurs und Kursänderung
            if((System.Windows.Application.Current as App).Person != null){
                if ((System.Windows.Application.Current as App).Person.Favorites != null)
                {
                    foreach (Aktie ak in (System.Windows.Application.Current as App).Person.Favorites)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = ak.Name;
                        dr1[1] = ak.Abbreviation;
                        dr1[2] = ak.Course;
                        dt.Rows.Add(dr1);
                    }
                }
            }
            AktienListe.ItemsSource = dt.DefaultView;

        }


        Aktie getSelectedAKtie (String Abbriviation)
        {
            Aktie a = new Aktie();
            foreach(Aktie ak in (System.Windows.Application.Current as App).Person.Favorites)
            {
                if (ak.Abbreviation == Abbriviation)
                {
                    a = ak;
                    break;
                }
            }
            return a;
        }
        void passViewZurAktieAn()
        {
            AktienInfo.Text = "";
            SelectedAktieName.Text = "";
            SelectedAktieName.Text = selectedAktie.Name;
            String akInfo = selectedAktie.Abbreviation.ToString();
            //Nur Name der Firma 
            //Nur News:
            //[
            //title,
            //link,
            //content, --> HTML text
            //timestamp
            //]
            //Webbrowser: 3,1 --> mittels Webbrowser html context darstellen

            //t1.Inlines.Add(h1);
            //Grid.SetRow(t1, 3);
            //Grid.SetColumn(t1, 1);

            string[] news = (System.Windows.Application.Current as App).th.getNews(selectedAktie);
            if (news.Length != 0) {
                SelectedAktieName.Text = news[0];
                Hyperlink h1 = new Hyperlink();
                h1.Inlines.Add(news[1]);
                h1.NavigateUri = new Uri(news[1]);
                h1.IsEnabled = true;
                AktienInfo.Inlines.Add(h1);
                Grid.SetRow(AktienInfo, 11);
                Grid.SetColumn(AktienInfo, 3);
                Grid.SetColumnSpan(AktienInfo, 4);
            }
            (System.Windows.Application.Current as App).th.getChartData(selectedAktie);
            //Chartvlues aus Aktie holen
            List<double> chartValues = selectedAktie.ChartValues;
            //diagramm1 = new DiagrammElement();
            DiagrammFav.addValue(chartValues);
            //InitializeComponent();
        }

        void kaufSpeziell(object sender, RoutedEventArgs e)
        {
            //Neues Funktion mit Anzahl
            Kaufen kaufenView = new Kaufen(selectedAktie);
            //kaufenView.
            kaufenView.ShowDialog();
        }

        void verkaufenSpeziell (object sender, RoutedEventArgs e)
        {
            Verkaufen verkaufenView = new Verkaufen(selectedAktie);
            verkaufenView.Show();
        }

        public Aktie SelectedAktie
        {
            get
            {
                return selectedAktie;
            }
        }

        void buildPage(object sender, EventArgs e)
        {
            Picture.Children.Clear();
            if ((System.Windows.Application.Current as App).isLoggedIn)
            {
                (System.Windows.Application.Current as App).Person = (System.Windows.Application.Current as App).th.updateUserInfo((System.Windows.Application.Current as App).Person);
                //Balance Platzieren
                BalanceLbl = new System.Windows.Controls.Label();
                //BalanceLbl.Content = "Guthaben: ";
                BalanceLbl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                BalanceLbl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid.SetRow(BalanceLbl, 2);
                Grid.SetRowSpan(BalanceLbl, 2);
                Grid.SetColumn(BalanceLbl, 6);
                BalanceLbl.Content += (System.Windows.Application.Current as App).Person.Balance.ToString();


                header = new System.Windows.Controls.Label();
                header.FontWeight = FontWeights.Bold;

                //AktienInfo = new System.Windows.Controls.Label();
                //Thickness marginInfo = AktienInfo.Margin;
                //marginInfo.Left = 10;
                //AktienInfo.Margin = marginInfo;
                //SelectedAktieName = new System.Windows.Controls.Label();

                //SelectedAktieName.FontWeight = FontWeights.Bold;

                header.Content = "Aktien in den Favoriten";
                Grid.SetRow(header, 8);
                Grid.SetColumn(header, 1);
                //Tabelle erzeúgen:
                //1. DataGrid erstellen
                AktienListe = new System.Windows.Controls.DataGrid();
                AktienListe.IsReadOnly = true;
                AktienListe.SelectionMode = (DataGridSelectionMode)System.Windows.Controls.SelectionMode.Single;
                AktienListe.MinColumnWidth = 200;
                //AktienListe.ColumnWidth = "0.25*";
                Grid.SetRow(AktienListe, 9);
                Grid.SetColumn(AktienListe, 1);
                Grid.SetRowSpan(AktienListe, 4);


                //2. DataTable erstellen
                DataTable dt = creatingDataGrid();
                //3. DataGridbefüllen
                fillingDataGrid(dt);
                Thickness margin = SelectedAktieName.Margin;
                margin.Left = 10;
                SelectedAktieName.Margin = margin;
                //AktienListe.SelectionChanged += new SelectionChangedEventHandler(Listbox_SelectedValueChanged);
                //AktienListe.MouseDoubleClick += new MouseButtonEventHandler(mouseClick);
                AktienListe.SelectionChanged += new SelectionChangedEventHandler(selectionChanged);
                //l1.ItemsSource = (Application.Current as App).Person.Aktien[0].Name;
                //fllingListbox(AktienListe);

                //Diagramm setzen

                Grid.SetRow(AktienInfo, 11);
                Grid.SetColumn(AktienInfo, 3);
                Grid.SetColumnSpan(AktienInfo, 4);

                Grid.SetRow(SelectedAktieName, 10);
                Grid.SetColumn(SelectedAktieName, 3);
                Grid.SetColumnSpan(SelectedAktieName, 4);
                Picture.Children.Add(AktienInfo);
                Picture.Children.Add(header);
                Picture.Children.Add(AktienListe);
                Picture.Children.Add(SelectedAktieName);
                Picture.Children.Add(BalanceLbl);

                //d1 = new DiagrammElement();
            }
        }
        void buildPageFct()
        {
            if ((System.Windows.Application.Current as App).isLoggedIn)
            {
                (System.Windows.Application.Current as App).Person = (System.Windows.Application.Current as App).th.updateUserInfo((System.Windows.Application.Current as App).Person);
                //Balance Platzieren
                BalanceLbl = new System.Windows.Controls.Label();
                //BalanceLbl.Content = "Guthaben: ";
                BalanceLbl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                BalanceLbl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid.SetRow(BalanceLbl, 2);
                Grid.SetColumn(BalanceLbl, 6);
                Grid.SetRowSpan(BalanceLbl, 2);
                BalanceLbl.Content += (System.Windows.Application.Current as App).Person.Balance.ToString();

                header = new System.Windows.Controls.Label();
                header.FontWeight = FontWeights.Bold;
                AktienInfo = new System.Windows.Controls.TextBlock();
                Thickness marginInfo = AktienInfo.Margin;
                marginInfo.Left = 10;
                AktienInfo.Margin = marginInfo;
                AktienInfo.TextWrapping = TextWrapping.WrapWithOverflow;
                SelectedAktieName = new System.Windows.Controls.TextBlock();
                SelectedAktieName.TextWrapping = TextWrapping.WrapWithOverflow;

                SelectedAktieName.FontWeight = FontWeights.Bold;

                header.Content = "Aktien in den Favoriten";
                Grid.SetRow(header, 8);
                Grid.SetColumn(header, 1);
                //Tabelle erzeúgen:
                //1. DataGrid erstellen
                AktienListe = new System.Windows.Controls.DataGrid();
                AktienListe.IsReadOnly = true;
                AktienListe.SelectionMode = (DataGridSelectionMode)System.Windows.Controls.SelectionMode.Single;
                AktienListe.MinColumnWidth = 200;
                //AktienListe.ColumnWidth = "0.25*";
                Grid.SetRow(AktienListe, 9);
                Grid.SetColumn(AktienListe, 1);
                Grid.SetRowSpan(AktienListe, 4);

                Grid.SetRow(SelectedAktieName, 10);
                Grid.SetColumn(SelectedAktieName, 3);
                Grid.SetColumnSpan(SelectedAktieName, 4);


                Grid.SetRow(AktienInfo, 11);
                Grid.SetColumn(AktienInfo, 3);
                Grid.SetColumnSpan(AktienInfo, 4);


                //2. DataTable erstellen
                DataTable dt = creatingDataGrid();
                //3. DataGridbefüllen
                fillingDataGrid(dt);
                Thickness margin = SelectedAktieName.Margin;
                margin.Left = 10;
                SelectedAktieName.Margin = margin;
                //AktienListe.SelectionChanged += new SelectionChangedEventHandler(Listbox_SelectedValueChanged);
                //AktienListe.MouseDoubleClick += new MouseButtonEventHandler(mouseClick);
                AktienListe.SelectionChanged += new SelectionChangedEventHandler(selectionChanged);
                //l1.ItemsSource = (Application.Current as App).Person.Aktien[0].Name;
                //fllingListbox(AktienListe);

                //Diagramm setzen


                //Wbesite Anzeigen

                //Grid.SetRow(SelectedAktieName, 2);
                //Grid.SetColumn(SelectedAktieName, 1);
                Picture.Children.Add(header);
                Picture.Children.Add(AktienListe);
                Picture.Children.Add(AktienInfo);
                Picture.Children.Add(SelectedAktieName);
                Picture.Children.Add(BalanceLbl);

                //diagramm1 = new DiagrammElement();
                //diagramm1.loadChart();
            }
        }
    }
    public class Holder
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Point Point { get; set; }

        public Holder()
        {
        }
    }

    public class Value
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Value(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
