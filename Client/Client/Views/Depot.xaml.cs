﻿using Client.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLogic;
using Client.ViewModels;
using System.Windows.Threading;
using LiveCharts;
using LiveCharts.Wpf;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Depot.xaml
    /// </summary>
    public partial class Depot : UserControl
    {
        private int heightElement = 23;
        Label header;
        //Wenn es geht Aktieninfo als ListView
        DataGrid AktienListe;
        System.Windows.Controls.TextBlock AktienInfo;
        TextBox SelectedAktieName;
        int selectedIndexOld = -2;
        public Aktie selectedAktie;
        Button kaufenSpeziell;
        Button verkaufSpeziell;
        Label BalanceLbl;
        
        public Depot()
        {
            InitializeComponent();
            buildPageFct();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds((Application.Current as App).refreshFreshhold);
            //timer.Tick += buildPage;
            timer.Start();
            //plotter.Visibility = Visibility.Collapsed;
           
        }

        private void buildPageFct()
        {
            if ((Application.Current as App).isLoggedIn)
            {
                //plotter.Visibility = Visibility.Visible;
                (System.Windows.Application.Current as App).Person = (System.Windows.Application.Current as App).th.updateUserInfo((System.Windows.Application.Current as App).Person);
                BalanceLbl = new System.Windows.Controls.Label();
                BalanceLbl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                BalanceLbl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                Grid.SetRow(BalanceLbl, 2);
                Grid.SetRowSpan(BalanceLbl, 2);
                Grid.SetColumn(BalanceLbl, 6);
                BalanceLbl.Content += (System.Windows.Application.Current as App).Person.Balance.ToString();


                header = new Label();
                header.FontWeight = FontWeights.Bold;
                AktienInfo = new System.Windows.Controls.TextBlock();
                Thickness marginInfo = AktienInfo.Margin;
                marginInfo.Left = 10;
                AktienInfo.Margin = marginInfo;
                AktienInfo.TextWrapping = TextWrapping.WrapWithOverflow;
                SelectedAktieName = new TextBox();
                SelectedAktieName.TextWrapping = TextWrapping.WrapWithOverflow;

                SelectedAktieName.FontWeight = FontWeights.Bold;

                header.Content = "Aktien im Depot";
                Grid.SetRow(header, 8);
                Grid.SetColumn(header, 1);
                //Tabelle erzeúgen:
                //1. DataGrid erstellen
                AktienListe = new DataGrid();
                AktienListe.IsReadOnly = true;
                AktienListe.SelectionMode = (DataGridSelectionMode)SelectionMode.Single;
                AktienListe.MinColumnWidth = 200;
                //AktienListe.ColumnWidth = "0.25*";
                Grid.SetRow(AktienListe, 9);
                Grid.SetColumn(AktienListe, 1);
                Grid.SetRowSpan(AktienListe, 4);


                //2. DataTable erstellen
                DataTable dt = creatingDataGrid();
                //3. DataGridbefüllen
                fillingDataGrid(dt);
                Thickness margin = SelectedAktieName.Margin;
                margin.Left = 10;
                SelectedAktieName.Margin = margin;
                //AktienListe.SelectionChanged += new SelectionChangedEventHandler(Listbox_SelectedValueChanged);
                //AktienListe.MouseDoubleClick += new MouseButtonEventHandler(mouseClick);
                AktienListe.SelectionChanged += new SelectionChangedEventHandler(selectionChanged);
                //l1.ItemsSource = (Application.Current as App).Person.Aktien[0].Name;
                //fllingListbox(AktienListe);



                Grid.SetRow(AktienInfo, 11);
                Grid.SetColumn(AktienInfo, 3);
                Grid.SetColumnSpan(AktienInfo, 4);

                Grid.SetRow(SelectedAktieName, 10);
                Grid.SetColumn(SelectedAktieName, 3);
                Grid.SetColumnSpan(SelectedAktieName, 4);
                Picture.Children.Add(AktienInfo);
                Picture.Children.Add(header);
                Picture.Children.Add(AktienListe);
                Picture.Children.Add(SelectedAktieName);
                Picture.Children.Add(BalanceLbl);
            }
        }

        private void buildPage(object sender, EventArgs e)
        {
            if ((Application.Current as App).isLoggedIn)
            {
                (System.Windows.Application.Current as App).Person = (System.Windows.Application.Current as App).th.updateUserInfo((System.Windows.Application.Current as App).Person);
                //plotter.Visibility = Visibility.Visible;
                header = new Label();
                header.FontWeight = FontWeights.Bold;
                AktienInfo = new System.Windows.Controls.TextBlock();
                Thickness marginInfo = AktienInfo.Margin;
                marginInfo.Left = 10;
                AktienInfo.Margin = marginInfo;
                SelectedAktieName = new TextBox();

                SelectedAktieName.FontWeight = FontWeights.Bold;

                header.Content = "Aktien im Depot";
                Grid.SetRow(header, 8);
                Grid.SetColumn(header, 1);
                //Tabelle erzeúgen:
                //1. DataGrid erstellen
                AktienListe = new DataGrid();
                AktienListe.IsReadOnly = true;
                AktienListe.SelectionMode = (DataGridSelectionMode)SelectionMode.Single;
                AktienListe.MinColumnWidth = 200;
                //AktienListe.ColumnWidth = "0.25*";
                Grid.SetRow(AktienListe, 9);
                Grid.SetColumn(AktienListe, 1);
                Grid.SetRowSpan(AktienListe, 4);


                //2. DataTable erstellen
                DataTable dt = creatingDataGrid();
                //3. DataGridbefüllen
                fillingDataGrid(dt);
                Thickness margin = SelectedAktieName.Margin;
                margin.Left = 10;
                SelectedAktieName.Margin = margin;
                //AktienListe.SelectionChanged += new SelectionChangedEventHandler(Listbox_SelectedValueChanged);
                //AktienListe.MouseDoubleClick += new MouseButtonEventHandler(mouseClick);
                AktienListe.SelectionChanged += new SelectionChangedEventHandler(selectionChanged);
                //l1.ItemsSource = (Application.Current as App).Person.Aktien[0].Name;
                //fllingListbox(AktienListe);



                Grid.SetRow(AktienInfo, 11);
                Grid.SetColumn(AktienInfo, 3);
                Grid.SetColumnSpan(AktienInfo, 4);

                Grid.SetRow(SelectedAktieName, 10);
                Grid.SetColumn(SelectedAktieName, 3);
                Grid.SetColumnSpan(SelectedAktieName, 4);
                Picture.Children.Add(AktienInfo);
                Picture.Children.Add(header);
                Picture.Children.Add(AktienListe);
                Picture.Children.Add(SelectedAktieName);

            }
        }

        private void fillingDataGrid(DataTable dt)
        {

            if ((Application.Current as App).Person != null)
            {
                if ((Application.Current as App).Person.Depot != null)
                {

                    foreach (Aktie ak in (Application.Current as App).Person.Depot)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = ak.Name;
                        dr1[1] = ak.Abbreviation;
                        dr1[2] = ak.Course;
                        dr1[3] = ak.Anzahl;
                        dt.Rows.Add(dr1);
                    }
                }
            }
            AktienListe.ItemsSource = dt.DefaultView;

        }
        private DataTable creatingDataGrid()
        {
            DataTable dt = new DataTable();
            DataColumn AktienName = new DataColumn("Name", typeof(string));
            DataColumn AktienKürzel = new DataColumn("Kürzel", typeof(string));
            DataColumn Kurs = new DataColumn("Kurs", typeof(double));
            DataColumn AnzahlAktien = new DataColumn("Anzahl", typeof(int));
            dt.Columns.Add(AktienName);
            dt.Columns.Add(AktienKürzel);
            dt.Columns.Add(Kurs);
            dt.Columns.Add(AnzahlAktien);
            return dt;
        }
        private void selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AktienInfo.Text = "";
            SelectedAktieName.Text = "";
            DataGrid dG = sender as DataGrid;
            if (dG.SelectedItem == null)
            {
                kaufenSpeziell.Visibility = Visibility.Collapsed;
            }
            //else if (selectedIndexOld == lb.SelectedIndex)
            //{
            //    kaufenSpeziell.Visibility = Visibility.Collapsed;
            //    lb.SelectedIndex = -1;
            //    SelectedAktieName.Content = "";
            //    selectedIndexOld = -2;
            //    //nur Kaufen button anzeigen --> neues Pop Up
            //    //Event für Button

            //    Grid.SetRow(kaufenSpeziell, 1);
            //    Grid.SetColumn(kaufenSpeziell, 1);
            //    //Picture.Children.Add(verkaufen);
            //    //Picture.Children.Add(kaufenSpeziell);

            //}
            else
            {
                //Aktie Darstellen


                selectedIndexOld = dG.SelectedIndex;
                DataRowView d1 = (DataRowView)dG.SelectedItem;

                selectedAktie = getSelectedAKtie(d1.Row.ItemArray[0].ToString());
                //Aktieninfo als Grid / View darstellen
                passViewZurAktieAn();

                //Kaufen/ Button anzeigen
                kaufenSpeziell = new Button();
                kaufenSpeziell.Click += new RoutedEventHandler(kaufSpeziell);
                //Event für Button
                Thickness margin = kaufenSpeziell.Margin;
                margin.Left = 10;
                kaufenSpeziell.Margin = margin;
                kaufenSpeziell.VerticalAlignment = VerticalAlignment.Center;
                kaufenSpeziell.VerticalContentAlignment = VerticalAlignment.Center;
                kaufenSpeziell.HorizontalAlignment = HorizontalAlignment.Left;
                kaufenSpeziell.Content = "Kaufen";
                kaufenSpeziell.Height = heightElement;

                //Verkaufen Button
                verkaufSpeziell = new Button();
                verkaufSpeziell.Click += new RoutedEventHandler(verkaufenSpeziell);
                verkaufSpeziell.Margin = margin;
                verkaufSpeziell.VerticalAlignment = VerticalAlignment.Center;
                verkaufSpeziell.VerticalContentAlignment = VerticalAlignment.Center;
                verkaufSpeziell.HorizontalAlignment = HorizontalAlignment.Right;
                verkaufSpeziell.Content = "Verkaufen";
                verkaufSpeziell.Height = heightElement;





                Grid.SetRow(verkaufSpeziell, 12);
                Grid.SetColumn(verkaufSpeziell, 3);
                Grid.SetColumnSpan(verkaufSpeziell, 4);
                Grid.SetRow(kaufenSpeziell, 12);
                Grid.SetColumn(kaufenSpeziell, 3);
                Grid.SetColumnSpan(kaufenSpeziell, 4);
                Picture.Children.Add(kaufenSpeziell);
                Picture.Children.Add(verkaufSpeziell);
            }
            //lb.SelectedIndex = -1;

        }
        Aktie getSelectedAKtie(String Name)
        {
            Aktie a = new Aktie();
            foreach (Aktie ak in (Application.Current as App).Person.Depot)
            {
                if (ak.Name == Name)
                {
                    a = ak;
                }
            }
            return a;
        }
        void passViewZurAktieAn()
        {
            AktienInfo.Text = "";
            SelectedAktieName.Text = "";
            SelectedAktieName.Text = selectedAktie.Name;
            String akInfo = selectedAktie.Abbreviation.ToString();
            string[] news = (System.Windows.Application.Current as App).th.getNews(selectedAktie);
            if (news.Length != 0)
            {
                SelectedAktieName.Text = news[0];
                Hyperlink h1 = new Hyperlink();
                h1.Inlines.Add(news[1]);
                h1.NavigateUri = new Uri(news[1]);
                h1.IsEnabled = true;
                AktienInfo.Inlines.Add(h1);
                Grid.SetRow(AktienInfo, 11);
                Grid.SetColumn(AktienInfo, 3);
                Grid.SetColumnSpan(AktienInfo, 4);
            }
            (System.Windows.Application.Current as App).th.getChartData(selectedAktie);
            //Chartvlues aus Aktie holen
            List<double> chartValues = selectedAktie.ChartValues;
            //diagramm1 = new DiagrammElement();
            DiagrammDepot.addValue(chartValues);


        }
        void kaufSpeziell(object sender, RoutedEventArgs e)
        {
            //Neues Funktion mit Anzahl
            Kaufen kaufenView = new Kaufen(selectedAktie); ;
            //kaufenView.
            kaufenView.ShowDialog();
        }
        void verkaufenSpeziell(object sender, RoutedEventArgs e)
        {
            Verkaufen verkaufenView = new Verkaufen(selectedAktie);
            verkaufenView.ShowDialog();
        }

        void drawGraph()
        {

        }
    }
}
