﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Client.Commands;
using Client.ViewModels;
using Client.Data;
using ClientLogic;
using System.Windows.Threading;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        
        public Login()
        {
            
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds((Application.Current as App).refreshFreshhold);
            timer.Tick += buildPage;
            timer.Start();
            
            DataContext = new LoginViewModel();
            isVisible_LoggedIn = true;
        }

        private void buildPage(object sender, EventArgs e)
        {
            if ((Application.Current as App).isLoggedIn)
            {
                Picture.Children.Clear();
            }
        }
        private Button logInBtnFinal;
        public bool isVisible_LoggedIn;

        public bool IsVisibleN
        {
            get;
            set;
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            //Überprüfung mit DB Connector ob Email Adresse existiert
            bool eMailExists;
            bool anMeldung = true;
            String eMail;
            String password;
            InputEMail.Text = InputEMail.Text.Trim();
            if (String.IsNullOrEmpty(InputEMail.Text) == false)
            {
                    //Steuerelement grafisch Anpassen in xaml datei
                    //Input Ändern zu Passwort und Registrierungsbutton verbergen
                    //Vorhergehendes Element löschen 
                    IsVisibleN = false;
                bool test = InputEMail.IsVisible;
                //Check Passwort
                if (String.IsNullOrEmpty(InputPasswort2.Password.ToString()))
                {
                    MessageBox.Show("Bitte geben Sie ein Passwort ein");
                    anMeldung = false;
                }
                else
                {
                    password = InputPasswort2.Password.ToString();
                    eMail = InputEMail.Text.Trim();
                    //Credationls überprüfen mittels Passwort über Objekt Objekt
                    //Person p1 = new Person (e-mail, passwort);
                    TransactionHandler th = new TransactionHandler();
                    anMeldung = th.CheckCredentials("pascal.fabiano@python.fangirl", "pfabiano");
                    if (anMeldung == true)
                    {
                        (Application.Current as App).isLoggedIn = true;
                        Person p1 = th.getUser("pascal.fabiano@python.fangirl", "pfabiano");
                        (Application.Current as App).Person = p1;
                        (Application.Current as App).th = new TransactionHandler();
                        //Main View Benachrichtigen
                        //weiter´leitung zum Depot über 
                        //CommandBinding commandBinding = new CommandBinding(viemode)
                    }
                    else
                    {
                        MessageBox.Show("Ihr Passwort ist Falsch");
                    }
                }
            }
            else
            {
                MessageBox.Show("Bitte Geben Sie eine E-Mail ein");
            }
        }

        
        private void checkComponents()
        {
            if ((Application.Current as App).isLoggedIn)
            {
                isVisible_LoggedIn = false;
            }
        }
        private void PW_First_Got_FocusedLogin(object sender, RoutedEventArgs e)
        {
            InputPasswort2.Password = InputPasswort2.Password.Trim();
            InputPasswort2.Visibility = Visibility.Visible;
            InputPasswort2.Foreground = Brushes.Black;
            //Lösche das alte Passwort
            InputPasswort2.Password = "";
            InputPasswort2.Focus();
        }
        private void PW_First_Lost_FocusedLogin(object sender, RoutedEventArgs e)
        {
            InputPasswort2.Password = InputPasswort2.Password.Trim();
            if (string.IsNullOrEmpty(InputPasswort2.Password))
            {
                //InputBrutto.Visibility = Visibility.Collapsed;
                //waterMarkedText.Visibility = Visibility.Visible;
                InputPasswort2.Password = "1234";
                InputPasswort2.Foreground = Brushes.Gray;

            }
            else
            {
                InputPasswort2.Foreground = Brushes.Black;
            }
        }

        private void PW_First_Got_FocusedReg(object sender, RoutedEventArgs e)
        {
            InputPasswortReg.Password = InputPasswortReg.Password.Trim();
            InputPasswortReg.Visibility = Visibility.Visible;
            //Lösche das alte Passwort
            InputPasswortReg.Password = "";
            InputPasswortReg.Focus();
        }

        private void PW_First_Lost_FocusReg(object sender, RoutedEventArgs e)
        {
            InputPasswortReg.Password = InputPasswortReg.Password.Trim();
            if (string.IsNullOrEmpty(InputPasswortReg.Password))
            {
                //InputBrutto.Visibility = Visibility.Collapsed;
                //waterMarkedText.Visibility = Visibility.Visible;
                InputPasswortReg.Password = "1234";
                InputPasswortReg.Foreground = Brushes.Gray;


            }
            else
            {
                InputPasswortReg.Foreground = Brushes.Black;
            }
        }
        private void PW_Second_Got_FocusedReg(object sender, RoutedEventArgs e)
        {
            InputPasswortAgainReg.Password = InputPasswortAgainReg.Password.Trim();
            InputPasswortAgainReg.Visibility = Visibility.Visible;
            //Lösche das alte Passwort
            InputPasswortAgainReg.Password = "";
            InputPasswortAgainReg.Focus();
        }
        private void PW_Second_Lost_FocusedReg(object sender, RoutedEventArgs e)
        {
            InputPasswortAgainReg.Password = InputPasswortAgainReg.Password.Trim();
            if (string.IsNullOrEmpty(InputPasswortAgainReg.Password))
            {
                //InputBrutto.Visibility = Visibility.Collapsed;
                //waterMarkedText.Visibility = Visibility.Visible;
                InputPasswortAgainReg.Password = "1234";
                InputPasswortAgainReg.Foreground = Brushes.Gray;

            }
            else
            {
                InputPasswortAgainReg.Foreground = Brushes.Black;
            }
        }

        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            InputVornameReg.Text = InputVornameReg.Text.Trim();
            InputNachneReg.Text = InputNachneReg.Text.Trim();
            InputEMailReg.Text = InputEMailReg.Text.Trim();
            if (String.IsNullOrEmpty(InputVornameReg.Text) || String.IsNullOrEmpty(InputNachneReg.Text) || String.IsNullOrEmpty(InputEMailReg.Text) || String.IsNullOrEmpty(InputPasswortReg.Password) || String.IsNullOrEmpty(InputPasswortAgainReg.Password))
            {
                MessageBox.Show("Bitte füllen Sie das formular vollständig aus");
            }
            else if (InputPasswortReg.Password != InputPasswortAgainReg.Password)
            {
                MessageBox.Show("Bitte geben Sie zur Betsätigung Ihr Passwort zwei Mal gleich ein");
            }
            else
            {
                //Get Values
                String vorname = InputVornameReg.Text.Trim();
                String nachname = InputNachneReg.Text.Trim();
                String e_mail = InputEMailReg.Text.Trim();
                String passwort = InputPasswortReg.Password.ToString();
                //Neues Personen Objekt erstellen und senden
                //Anwort vom server erwarten
                
                TransactionHandler th = new TransactionHandler();
                bool reg = th.registerUser(vorname, nachname, e_mail, passwort);
                if (reg)
                {
                    MessageBox.Show("Ihre Registrierung war erfolgreich bitte Loggen Sie sich ein");
                    (Application.Current as App).isLoggedIn = false;
                }
                else
                {
                    MessageBox.Show("Ihre Registrierung war nicht erfolgreich");
                    (Application.Current as App).isLoggedIn = false;

                }
            }
        }
    }
}
