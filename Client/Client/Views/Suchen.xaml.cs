﻿using Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLogic;
using System.Data;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Suchen.xaml
    /// </summary>
    public partial class Suchen : UserControl
    {
        private int heightElement = 23;
        Aktie selectedAktie;
        Button zuFavoritenHinzufügen;
        DataGrid gefundeneAKtien;
        DataTable dt;
        List<Aktie> gefundeneAktien;
        public int HeightElement
        {
            get
            {
                return heightElement;
            }
        }
        public Suchen()
        {
            
            
            if ((Application.Current as App).isLoggedIn)
            {
                //Suchen nach AKtien auch möglich wenn man nichr angemeldet ist ?--> dann z.B. ohne Button zu favoriten hinzufügen
                InitializeComponent();
                gefundeneAKtien = new DataGrid();
                gefundeneAKtien.SelectionChanged += new SelectionChangedEventHandler(GefundeneAKtien_SelectionChanged);
                Thickness margin = gefundeneAKtien.Margin;
                margin.Left = 15;
                margin.Right = 15;
                margin.Top = 15;
                margin.Bottom = 15;
                gefundeneAKtien.Margin = margin;
                Grid.SetRow(gefundeneAKtien, 3);
                Grid.SetColumn(gefundeneAKtien, 1);
                Picture.Children.Add(gefundeneAKtien);
                //gefundeneAKtien.Visibility = Visibility.Collapsed;
                //2. DataTable erstellen
                dt = creatingDataGrid();

            }
        }
        DataTable creatingDataGrid()
        {
            DataTable dt = new DataTable();
            DataColumn AktienName = new DataColumn("Name", typeof(string));
            DataColumn AktienKürzel = new DataColumn("Kürzel", typeof(string));
            DataColumn Kurs = new DataColumn("Börse", typeof(string));

            dt.Columns.Add(AktienName);
            dt.Columns.Add(AktienKürzel);
            dt.Columns.Add(Kurs);
            return dt;
        }
        void suchenBtnClick(object sender, RoutedEventArgs e)
        {
            //Bekommt liste mit Key Symbolen zurück
            infoAktie.Content = "";
            //zuFavoritenHinzufügen = null;
            if (zuFavoritenHinzufügen != null)
            {
                zuFavoritenHinzufügen.Visibility = Visibility.Collapsed;
            }
            //zuFavoritenHinzufügen = null;
            //gefundeneAKtien.Items.Clear();
            String inputAktie = InputAktie.Text.ToString();
            //(Application.Current as App).th.
            //(Application.Current as App).th.
            gefundeneAktien = (Application.Current as App).th.SearchKeywordErgebnisse(inputAktie);
            if (gefundeneAktien.Count != 0)
            {
                //3. DataGridbefüllen
                fillingDataGrid(dt, gefundeneAktien);
                //Aktieninfos darstellen
                gefundeneAKtien.Visibility = Visibility.Visible;
            }
            else
            {
                gefundeneAKtien.Visibility = Visibility.Collapsed;
                MessageBox.Show("Zu dem eingebenen Kürzel konnte keine Aktie gefunden werden !\n Bitt überprüfen Sie Ihre Eingabe");
            }
             
            //Aktie mittels RegEx suchen / holen 
        }

        private void fillingDataGrid(DataTable dt, List<Aktie> gefundeneAktie)
        {
            //Kurs und Kursänderung
            foreach (Aktie ak in gefundeneAktie)
            {
                DataRow dr1 = dt.NewRow();
                dr1[0] = ak.Name;
                dr1[1] = ak.Abbreviation;
                dr1[2] = ak.Boersenname;
                dt.Rows.Add(dr1);

            }
            gefundeneAKtien.ItemsSource = dt.DefaultView;

        }

        private void GefundeneAKtien_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.DataGrid dG = sender as System.Windows.Controls.DataGrid;
            if (dG != null)
            {
                DataRowView d1 = (DataRowView)dG.SelectedItem;
                selectedAktie = getSelectedAKtie(d1.Row.ItemArray[1].ToString());
                    //displayAktienInfo(selectedAktie);

                    zuFavoritenHinzufügen = new Button();
                    zuFavoritenHinzufügen.Content = "Zu favoriten hinzufügen";
                    zuFavoritenHinzufügen.Click += new RoutedEventHandler(btnClickZuFavoriten);
                    zuFavoritenHinzufügen.HorizontalAlignment = HorizontalAlignment.Right;
                    zuFavoritenHinzufügen.VerticalAlignment = VerticalAlignment.Top;
                    Thickness margin = zuFavoritenHinzufügen.Margin;
                    margin.Left = 12;
                    margin.Top = 12;
                    zuFavoritenHinzufügen.Margin = margin;


                    Grid.SetRow(zuFavoritenHinzufügen, 2);
                    Grid.SetColumn(zuFavoritenHinzufügen, 1);
                    Picture.Children.Add(zuFavoritenHinzufügen);
            }
        }

        Aktie getSelectedAKtie(String Name)
        {
            Aktie a = new Aktie();
           foreach(Aktie ak in gefundeneAktien)
            {
                if(ak.Abbreviation == Name)
                {
                    a = ak;
                }
            }
            return a;
        }

        void btnClickZuFavoriten(object sender, RoutedEventArgs e)
        {
            //Zu favoriten Hinzufügen
            if((Application.Current as App).th.favStonk(((Application.Current as App).Person), selectedAktie)){
                MessageBox.Show("Aktie wurde erfolgreich den Favoriten hinzugefügt");
            }
            else
            {
                MessageBox.Show("Es ist ein Fehler aufgetreten");
            }
        }
    }
}
