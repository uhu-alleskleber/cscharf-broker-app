﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using LiveCharts.Wpf.Charts.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für DiagrammElement.xaml
    /// </summary>
    public partial class DiagrammElement : UserControl
    {
        public ChartValues<ObservableValue> Values { get; set; }
        ChartValues<ObservableValue> v;
        public DiagrammElement()
        {
            InitializeComponent();
            chart.Series = new SeriesCollection { new LineSeries
            {
                Values = new ChartValues<double>()
            }
            };
        }

        public void addValue(List<double> test)
        {
            v = new ChartValues<ObservableValue>();
           
           foreach(int a in test)
            {
                v.Add(
                       new ObservableValue(a)
                    );
            }
            chart.Series[0].Values = v;
        }
    }
}
