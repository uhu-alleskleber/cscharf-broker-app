﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLogic;
using Client.Views;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Kaufen.xaml
    /// </summary>
    public partial class Kaufen : Window
    {
        private Aktie ak;
        public int anzahl;
        public String AktienName;
        public double AktienWert;
        public int Height = 23;
        public Kaufen()
        {
            InitializeComponent();
            //Aktuellen Aktienwert holen möglicherweise über Timer
        }
        public Kaufen(Aktie AK)
        {
            InitializeComponent();
            ak = AK;
            AktienNameLbL.Content = ak.Name;
            AktienWertLbL.Content = ak.Course;
            AktienKürzelLbL.Content = ak.Abbreviation;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Transaktion ausführen
            bool checkkauf;
            if (anzahl > 0)
            {
                checkkauf=(Application.Current as App).th.buyStonk((Application.Current as App).Person, ak, anzahl);
                if (checkkauf == true)
                {
                    this.Close();
                    MessageBox.Show("Transaktion ausgeführt");
                }
                else
                {
                    MessageBox.Show("Es ist ein Fehler aufgetreten");
                }
            }
            else
            {
                MessageBox.Show("Bitte geben Sie eine zahl größer 0 ein");
            }
        }

        private void AktienAnzahl_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Aktuelles Wert holen möglich
            double preisKauf = 0;
                 Int32.TryParse(AktienAnzahl.Text.ToString(), out anzahl);
            if (anzahl > 0)
            {
                preisKauf = anzahl * ak.Course;
                WertDesKaufes.Content = preisKauf.ToString();
                
            }
        }
        //Property changed event wenn sich textbox ändert
    }
}
