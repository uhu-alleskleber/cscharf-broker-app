﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Client.ViewModels;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Registrierung.xaml
    /// </summary>
    public partial class Registrierung : UserControl, INotifyPropertyChanged
    {
        private int heightElement = 23;
        public int HeightElement
        {
            get
            {
                return heightElement;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public Registrierung()
        {
            InitializeComponent();
            if ((Application.Current as App).isLoggedIn)
            {
                Picture.Children.Clear();
            }
            DataContext = new RegisterViewModel();
            InputPasswort.Password = "Test";
            InputPasswortAgain.Password = "Test";
        }
        public String UiD
        {
            get;
        }

        
        public string PlaceholderText { get; set; }
        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            InputVorname.Text= InputVorname.Text.Trim();
            InputNachne.Text= InputNachne.Text.Trim();
            InputEMail.Text=InputEMail.Text.Trim();
            if (String.IsNullOrEmpty(InputVorname.Text)|| String.IsNullOrEmpty(InputNachne.Text)|| String.IsNullOrEmpty(InputEMail.Text)||String.IsNullOrEmpty(InputPasswort.Password)||String.IsNullOrEmpty(InputPasswortAgain.Password))
            {
                MessageBox.Show("Bitte füllen Sie das formular vollständig aus");
            }
            else if (InputPasswort.Password != InputPasswortAgain.Password)
            {
                MessageBox.Show("Bitte geben Sie zur Betsätigung Ihr Passwort zwei Mal gleich ein");
            }
            else
            {
                //Get Values
                String vorname= InputVorname.Text.Trim();
                String nachname = InputNachne.Text.Trim();
                String e_mail = InputEMail.Text.Trim();
                String passwort = InputPasswort.Password.ToString();
                //Neues Personen Objekt erstellen und senden
                //Anwort vom server erwarten
                (Application.Current as App).isLoggedIn = true;
            }
        }

        private void PW_First_Got_Focused(object sender, RoutedEventArgs e)
        {
            InputPasswort.Password = InputPasswort.Password.Trim();
            InputPasswort.Visibility = Visibility.Visible;
            //Lösche das alte Passwort
            InputPasswort.Password = "";
            InputPasswort.Focus();
        }
        private void PW_First_Lost_Focused(object sender, RoutedEventArgs e)
        {
            InputPasswort.Password=InputPasswort.Password.Trim();
            if (string.IsNullOrEmpty(InputPasswort.Password))
            {
                //InputBrutto.Visibility = Visibility.Collapsed;
                //waterMarkedText.Visibility = Visibility.Visible;
                InputPasswort.Password = "1234";
                InputPasswort.Foreground = Brushes.Gray;


            }
            else
            {
                InputPasswort.Foreground = Brushes.Black;
            }
        }
        private void PW_Secónd_Got_Focused(object sender, RoutedEventArgs e)
        {
            InputPasswortAgain.Password = InputPasswortAgain.Password.Trim();
            InputPasswortAgain.Visibility = Visibility.Visible;
            //Lösche das alte Passwort
            InputPasswortAgain.Password = "";
            InputPasswortAgain.Focus();
        }
        private void PW_Second_Lost_Focused(object sender, RoutedEventArgs e)
        {
            InputPasswortAgain.Password = InputPasswortAgain.Password.Trim();
            if (string.IsNullOrEmpty(InputPasswortAgain.Password))
            {
                //InputBrutto.Visibility = Visibility.Collapsed;
                //waterMarkedText.Visibility = Visibility.Visible;
                InputPasswortAgain.Password = "1234";
                InputPasswortAgain.Foreground = Brushes.Gray;

            }
            else
            {
                InputPasswortAgain.Foreground = Brushes.Black;
            }
        }

        private void SourceUpdate(object sender, DataTransferEventArgs e)
        {

        }

        private void checkComponents()
        {
            if ((Application.Current as App).isLoggedIn)
            {
                Picture.Visibility = Visibility.Collapsed;
            }
        }
    }
}
