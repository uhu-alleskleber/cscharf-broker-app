﻿using Client.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientLogic;

namespace Client.Views
{
    /// <summary>
    /// Interaktionslogik für Verkaufen.xaml
    /// </summary>
    public partial class Verkaufen : Window
    {
        private int anzahl;
        private Aktie ak;
        public Verkaufen()
        {
            InitializeComponent();
            //Picture
            //Vorhande AktienANzahl holen und darstellen
        }
        public Verkaufen(Aktie aktie)
        {
            InitializeComponent();
            //aktuelle Kurs abfrage
            ak = aktie;
            AktName.Content = aktie.Name.ToString();
            AktKurs.Content = aktie.Course.ToString();
            verfügbareAnzhal.Content = aktie.Anzahl.ToString();
        }
        private void VerkaufenLoaded(object sender, RoutedEventArgs e)
        {

        }
        private void AktienAnzahl_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Aktuelles Wert holen möglich
            double preisKauf = 0;
            Int32.TryParse(gewünschteAnzahl.Text.ToString(), out anzahl);
            if (anzahl != 0)
            {
                preisKauf = anzahl * ak.Course;
                WertdesVerkaufes.Content = preisKauf.ToString();
            }
            if (anzahl > ak.Anzahl)
            {
                MessageBox.Show("Sie können nur maximale so viele Aktien verkaufen wie Sie besitzen");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            bool checkkauf;
            if (anzahl > ak.Anzahl)
            {
                MessageBox.Show("Sie können nur maximale so viele Aktien verkaufen wie Sie besitzen");
            }
            else
            {
                checkkauf = (Application.Current as App).th.sellStonk((Application.Current as App).Person, ak, anzahl);
                if (checkkauf == true)
                {
                    this.Close();
                    MessageBox.Show("Transaktion ausgeführt");
                }
                else
                {
                    MessageBox.Show("Es ist ein Fehler aufgetreten");
                }
            }
        }

    }
}
